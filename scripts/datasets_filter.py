"""Script that filters all EnviDat datasets in CKAN API and produces list
of names of datasets that meet filter condition. If no datasets match filter condition
then returns empty list.

Future development: New filter conditions can be supported and added to
    "condition" argument

Example command to run script from root directory:
    python -m scripts.datasets_filter --field subtitle --condition no_empty_string
"""

# Imports
import argparse
from ckanapi import RemoteCKAN
from app.remote_ckan import ckan_current_package_list_with_resources


def parse_arguments():
    """Parses arguments: field, condition, ckan_address"""

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--field",
        required=True,
        help="EnviDat dataset field that will have filter condition applied",
    )

    parser.add_argument(
        "--condition",
        choices=["no_empty_string"],
        required=True,
        help="Condition to use for filter, choice 'no_empty_string' means that the "
        "specified field is a string that cannot be an empty string",
    )

    parser.add_argument(
        "--ckan_address",
        default="https://www.envidat.ch",
        help="CKAN API host address. Default value is 'https://www.envidat.ch'",
    )

    return parser.parse_args()


def filter_no_empty_string(pkg: dict, field: str) -> str | None:
    """Filters a EnviDat package dictionary and returns "name" of package that does not
    have an empty string for the specified field.
    If package does not meet filter condition then returns None.

    Args:
        pkg (dict): EnviDat dataset dictionary
        field (str): EnviDat dataset field that will have filter condition applied
    """
    field_val = pkg.get(field)
    if isinstance(field_val, str) and len(field_val) > 0:
        return pkg.get("name")
    return None


def main():
    """Filters all EnviDat datasets in CKAN API and produces list of names of datasets
    that meet filter condition.
    If no datasets match filter condition then returns empty list.
    """
    args = parse_arguments()

    ckan = RemoteCKAN(args.ckan_address)
    ckan_pkgs = ckan_current_package_list_with_resources(ckan)

    match args.condition:
        case "no_empty_string":
            pkg_names = [filter_no_empty_string(pkg, args.field) for pkg in ckan_pkgs]
            return list(filter(lambda item: item is not None, pkg_names))


if __name__ == "__main__":
    print(main())
