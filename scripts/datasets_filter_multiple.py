"""Script that returns list of EnviDat datasets in CKAN API that meet filter conditions.

Optionally patches filtered datasets with data specified in JSON file.

CAUTION: It is highly recommended to first return list of packages that meet filter
conditions before patching datasets using the '--patch_json' option to guarantee
that only the desired packages will be updated!

Fields used for filters should be separated by a space and can only currently support
ONE filter per field. The example below returns packages that have organization "nfi"
and a maintainer "email" of "123@"wsl.ch"
Example: --fields organization_name=nfi maintainer_email=123@wsl.ch

Run the following command to see currently supported fields in the --fields section:
    python -m scripts.datasets_filter_multiple --help

Example command to run script from root directory to return a list of packages that meet
filter conditions in public API:
    python -m scripts.datasets_filter_multiple --fields organization_name=nfi

Example command that uses quotes to correctly parse argument values that have spaces:
    python -m scripts.datasets_filter_multiple --fields publisher="Swiss Federal Research Institute WSL"

Requires admin authorization to patch dataset.
Example: --authorization <envidat_cookie>

Example command to run script from root directory '--patch_json' option to patch data
that match filter conditions:
    python -m scripts.datasets_filter_multiple --fields organization_name=nfi maintainer_email=123@wsl.ch --authorization <envidat_cookie> --patch_json test.json

To see an example of a JSON file used to patch datasets see
    'app/tests/test_data/example_patch_data.json'
"""

# Imports
import argparse
import json
from pprint import pprint

from ckanapi import RemoteCKAN
from app.remote_ckan import ckan_current_package_list_with_resources, ckan_package_patch


class ParseKwargs(argparse.Action):
    """Class that parses kwargs in space separated list to a dictionary.
    Returns exception if there are duplicate keys"""

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, dict())

        for value in values:
            key, value = value.split("=")

            if key in (getattr(namespace, self.dest)).keys():
                raise Exception(f"Duplicate keys not allowed, check key: {key}")

            getattr(namespace, self.dest)[key] = value


def parse_arguments():
    """Parses command line arguments."""

    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter
    )

    parser.add_argument(
        "-c",
        "--ckan_address",
        default="https://www.envidat.ch",
        help="CKAN API host address. Default value is 'https://www.envidat.ch'",
    )

    parser.add_argument(
        "-f",
        "--fields",
        nargs="*",
        action=ParseKwargs,
        required=True,
        help="""
        EnviDat package fields that should be filtered to match the specified value. 
        
        Fields should be separated by a space and can only currently support one filter per field.
        
        Currently only supports the following fields: 
        
             organization_name (name of organization, example "gis")
             maintainer_email (email address of maintainer)
             publication_year (publication_year of publication)
             publication_year_multiple (list as semicolon separated string of publication_year of publication, must include quotes, example: "2023;2024;2025")
             publisher (publisher of publication)
             publisher_multiple (list as semicolon separated string of publisher of publication, must include quotes example: "EPFL;Remote Sensing;WSL")
             notes_contains (the substring passed is contained within the "notes" value)
             
             The following fields can also be reversed by adding a ! in front.
             e.g.  !publication_state="published" returns datasets that do not have the value "published"
             e.g.  publication_state="published" returns datasets that do have the value "published"
             
                 creator_user_id
                 doi
                 id
                 language
                 license_id
                 license_title
                 license_url
                 name
                 owner_org
                 publication_state
                 resource_type
                 resource_type_general
                 spatial_info
                 state
                 subtitle
                 title
                 type
                 version
            
        Also filters for DOIs that starts with the value passed for the argument 'doi_startswith'.
        Example usage: --fields doi_startswith=10.21258 
        Example returns all datasets that have a 'doi' values that starts with '10.2158'.
        
        Use quotes to correctly parse argument values that have spaces.
        Example usage: --fields  publisher="Swiss Federal Research Institute WSL"
        """,
    )

    parser.add_argument(
        "-a",
        "--authorization",
        help="""
        EnviDat CKAN cookie for logged in user passed in authorization header. 
        Requires admin authorization cookie to use '--patch_json' option to patch packages.
        """,
    )

    parser.add_argument(
        "--patch_json",
        help="""
        Path to JSON file that contains data that will be used to patch dataset(s) that match filter conditions.
        To see example of a JSON file used to patch datasets navigate to project file 'app/tests/test_data/example_patch_data.json'
        """,
    )

    return parser.parse_args()


def has_organization_name(pkg, organization_name) -> bool:
    """
    Return True if an EnviDat package dictionary has an organization name that matches
    input organization name string. Else returns False.
    """
    if pkg.get("organization").get("name") == organization_name:
        return True
    return False


def has_maintainer_email(pkg, maintainer_email) -> bool:
    """
    Return True if an EnviDat package dictionary has a maintainer email that matches
    input maintainer email string.
    Else returns False.
    """
    maintainer = json.loads(pkg.get("maintainer"))
    if maintainer.get("email") == maintainer_email:
        return True
    return False


def has_publisher(pkg, publisher) -> bool:
    """
    Return True if an EnviDat package dictionary has a publisher value in the
    "publication" field that matches input publisher string. Else returns False.
    """
    publication = json.loads(pkg.get("publication"))
    if publication.get("publisher") == publisher:
        return True
    return False


def has_publisher_multiple(pkg, publisher_multiple) -> bool:
    """
    Return True if an EnviDat package dictionary "publisher" value in the
    "publication" field matches one of the publishers in the
    input publisher_multiple string.
    Else returns False.

    publisher_multiple value must be passed as a semicolon separated string with quotes
    to correctly parse the years. For example:
       publisher_multiple="EPFL;Remote Sensing;Geographica Helvetica",
    """
    publication = json.loads(pkg.get("publication"))
    publisher = publication.get("publisher").strip()
    pubs = [item.strip() for item in publisher_multiple.split(";") if item.strip()]

    if publisher in pubs:
        return True

    return False


def has_publication_year(pkg, publisher_year) -> bool:
    """
    Return True if an EnviDat package dictionary has a publication year that
    matches input publisher_year string. Else returns False.
    """
    publication = json.loads(pkg.get("publication"))
    if publication.get("publication_year") == publisher_year:
        return True
    return False


def has_publication_year_multiple(pkg, publication_year_multiple) -> bool:
    """
    Return True if an EnviDat package dictionary "publication_year" value in the
    "publication" field matches one of the publication years in the input
    publication_year_multiple string.
    Else returns False.

    publication_year_multiple value must be passed as a semicolon separated string and
    must be surrounded by quotes to correctly parse the years. For example:
       publication_year_list="2023;2024;2025"
    """
    publication = json.loads(pkg.get("publication"))
    publisher_year = publication.get("publication_year").strip()
    years = [
        item.strip() for item in publication_year_multiple.split(";") if item.strip()
    ]

    if publisher_year in years:
        return True

    return False


def filter_kwargs(pkg: dict, **fields) -> str | None:
    """
    Filters a EnviDat package dictionary and returns "name" of package that matches
    value(s) for all the specified fields(s).
    If package does not meet filter condition then returns None.

    NOTE, currently ONLY supports the EnviDat fields mentioned in --help.

    Also filters for DOIs that starts with the value passed for the
    argument "doi_startswith". For example --fields doi_startswith=10.21258
    returns all datasets that have a "doi" values that starts with "10.2158".

    Args:
        pkg (dict): EnviDat dataset dictionary
        fields (dict): Dictionary of EnviDat fields that should be filtered to match the
         specified values.
    """

    matches = []
    field_names = (
        "creator_user_id",
        "doi",
        "id",
        "language",
        "license_id",
        "license_title",
        "license_url",
        "name",
        "owner_org",
        "publication_state",
        "resource_type",
        "resource_type_general",
        "spatial_info",
        "state",
        "subtitle",
        "title",
        "type",
        "version"
    )

    for field, filter_val in fields.items():
        match field:
            case "organization_name":
                matches.append(has_organization_name(pkg, filter_val))

            case "maintainer_email":
                matches.append(has_maintainer_email(pkg, filter_val))

            case "publication_year":
                matches.append(has_publication_year(pkg, filter_val))

            case "publication_year_multiple":
                matches.append(has_publication_year_multiple(pkg, str(filter_val)))

            case "publisher":
                matches.append(has_publisher(pkg, filter_val))

            case "publisher_multiple":
                matches.append(has_publisher_multiple(pkg, str(filter_val)))

            case field if field in field_names:
                matches.append(True if pkg.get(field) == filter_val else False)

            case field if field.startswith("!") and field[1:] in field_names:
                matches.append(False if pkg.get(field[1:]) == filter_val else True)

            case "doi_startswith":
                matches.append(True if pkg.get("doi").startswith(filter_val) else False)

            case "notes_contains":
                matches.append(True if filter_val in pkg.get("notes") else False)

            case _:
                raise Exception(f"The following filter is not supported: {field}")

    if len(matches) > 0 and all(matches):
        return pkg.get("name")

    return None


def main():
    """
    Returns list of package names that meet filter conditions.
    Patches EnviDat datasets in CKAN API that meet filter conditions
    if '--patch_json' argument passed.
    Requires dataset owner or admin authorization to patch datasets.
    """
    args = parse_arguments()

    ckan = RemoteCKAN(address=args.ckan_address, apikey=args.authorization)
    ckan_pkgs = ckan_current_package_list_with_resources(ckan)

    pkg_names = [filter_kwargs(pkg, **args.fields) for pkg in ckan_pkgs]
    pkgs_filtered = list(filter(lambda item: item is not None, pkg_names))
    print("\n*********************************************************")
    print(f"Number of packages that match filter conditions: {len(pkgs_filtered)}")
    print("*********************************************************\n")

    if args.patch_json:
        with open(args.patch_json) as file:
            data = json.load(file)

        for pkg in pkgs_filtered:
            ckan_package_patch(pkg, data, ckan)
            print(f"Patched package '{pkg}' with new data in file '{args.patch_json}'")

    return pkgs_filtered


if __name__ == "__main__":
    pprint(main())
