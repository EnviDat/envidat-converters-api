"""
Script that is used to produce a list of datasets meet specified filter condition.

Run the following command to see currently supported filter conditions:
    python -m scripts.datasets_stewardship --help

Example command to run script from root directory to return a list of packages that meet
filter condition '2' (unpublished datasets with DOIs reserved).
    python -m scripts.datasets_stewardship --filter 2

Requires admin authorization to access private CKAN API!
Example: --authorization <envidat_cookie>

To enable writing datasets to HTML file use '--write_html' flag
Example: --write_html

Example command to run script and write output to HTML:
    python -m scripts.datasets_stewardship --filter 2 --authorization <envidat_cookie> --write_html
"""

# Imports
import argparse
import json
from pprint import pprint

from ckanapi import RemoteCKAN
from app.remote_ckan import ckan_current_package_list_with_resources


def parse_arguments():
    """Parses command line arguments."""

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-f",
        "--filter",
        choices=[1, 2, 3, 4, 5],
        type=int,
        required=True,
        help="Condition to use for filter. "
        "1 - Published datasets that DO NOT have a DOI assigned yet. "
        "2 - Unpublished datasets with DOIs reserved. "
        "3 - Unpublished datasets without DOIs. "
        "4 - Published datasets that have values for both related_publications "
        "and related_datasets"
        "5 - Published datasets which have DataCredits for at least one author",
    )

    parser.add_argument(
        "-c",
        "--ckan_address",
        default="https://www.envidat.ch",
        help="CKAN API host address. Default value is 'https://www.envidat.ch'",
    )

    parser.add_argument(
        "-a",
        "--authorization",
        help="EnviDat CKAN cookie for logged in user passed in authorization header. "
        "Requires admin authorization cookie to use access private API",
    )

    parser.add_argument(
        "-w",
        "--write_html",
        action="store_true",
        help="Enable writing HTML file with links corresponding to filtered datasets",
    )

    parser.add_argument(
        "-d",
        "--directory_html",
        default=None,
        help="Directory that should contain HTML file with links corresponding to "
        "filtered datasets. If not included then writes HTML file in same "
        "directory that script is executed.",
    )

    try:
        return parser.parse_args()
    except argparse.ArgumentTypeError as e:
        print(f"Invalid argument {e}")


def is_empty_string(pkg, field):
    """
    Return True if EnviDat input package dictionary has an empty string corresponding to
    the input field. Else returns False.
    """
    field_val = pkg.get(field)
    if isinstance(field_val, str) and len(field_val) == 0:
        return True
    return False


def is_empty_string_or_none(pkg, field):
    """
    Return True if EnviDat input package dictionary has an empty string corresponding to
    the input field. Also returns True is field does not exist in pkg.
    Else returns False.
    """
    field_val = pkg.get(field)

    if field_val is None:
        return True

    if isinstance(field_val, str) and len(field_val) == 0:
        return True

    return False


def is_not_empty_string(pkg, field):
    """
    Return True if EnviDat input package dictionary has a string that is not empty
    corresponding to the input field. Else returns False.
    """
    field_val = pkg.get(field)
    if isinstance(field_val, str) and len(field_val) > 0:
        return True
    return False


def filter_conditions(pkg: dict, filter_option: str) -> str | None:
    """
    Filters a EnviDat package dictionary and returns "name" value of package that
    meets the specified filter condition.
    If package does not meet filter condition then returns None.

    Args:
        pkg (dict): EnviDat dataset dictionary
        filter_option (str): Filter option chosen.
    """

    matches = []

    match filter_option:
        # Published datasets that do not have a DOI assigned
        case 1:
            matches.append(
                True
                if is_empty_string(pkg, "doi")
                and pkg.get("publication_state") == "published"
                else False
            )

        # Unpublished datasets with DOIs reserved
        case 2:
            matches.append(
                True
                if is_not_empty_string(pkg, "doi")
                and pkg.get("publication_state") != "published"
                else False
            )

        # Unpublished datasets that do not have a DOI assigned
        case 3:
            matches.append(
                True
                if is_empty_string(pkg, "doi")
                and pkg.get("publication_state") != "published"
                else False
            )

    if 0 < len(matches) == matches.count(True):
        return pkg.get("name")

    return None


def filter_conditions_v2(pkg: dict, filter_option: str) -> str | None:
    """
    Filters a EnviDat package dictionary and returns "name" value of package that
    meets the specified filter condition.
    If package does not meet filter condition then returns None.
    Only supports filter_option 4 or greater.

    Args:
        pkg (dict): EnviDat dataset dictionary
        filter_option (str): Filter option chosen.
    """

    is_match = False

    match filter_option:
        # Published datasets that have values for both "related_publications" and
        # "related_datasets"
        case 4:
            if (
                pkg.get("publication_state") == "published"
                and not is_empty_string_or_none(pkg, "related_publications")
                and not is_empty_string_or_none(pkg, "related_datasets")
            ):
                is_match = True

        # Published datasets which have DataCredits for at least one author
        case 5:
            if pkg.get("publication_state") == "published":
                authors = json.loads(pkg.get("author"))
                for author in authors:
                    if len(author.get("data_credit", [])) > 0:
                        is_match = True
                        break

    if is_match:
        return pkg.get("name")

    return None


def write_html_dataset_links(
    pkgs_filtered: list[str],
    ckan_address: str,
    filter_option: str,
    directory_html: str = None,
):
    """
    Write HTML file with datasets links. Prints error message if fails to write file.

     Args:
        pkgs_filtered (list): List of names of EnviDat packages that meet filter
            conditions
        ckan_address (str): CKAN API host address
        filter_option (str): Filter option chosen
        directory_html (str | None): Directory that should contain HTML file.
            Default is None (then writes in same directory that script executed from)
    """
    dataset_links = "<ol>"

    for pkg in pkgs_filtered:
        link = f"{ckan_address}/dataset/{pkg}"
        dataset_links += f"<li><a href={link} target=_blank>{link}</a></li>"

    dataset_links += "</ol>"
    output_html = "output"
    print("\n")

    match filter_option:
        case 1:
            print("Filter option 1: Published datasets that do not have a DOI assigned")
            output_html = "published_no_dois"
        case 2:
            print("Filter option 2: Unpublished datasets with DOIs reserved")
            output_html = "unpublished_reserved_dois"
        case 3:
            print(
                "Filter option 3: Unpublished datasets that "
                "do not have a DOI assigned"
            )
            output_html = "unpublished_no_dois"
        case 4:
            print(
                "Filter option 4: Published datasets that have values for both "
                "related_publications and related_datasets "
            )
            output_html = "published-related_publications-related_datasets"
        case 5:
            print(
                "Filter option 5: Published datasets which have DataCredits for at "
                "least one author"
            )
            output_html = "published-DataCredits"

    if directory_html:
        output_path = f"{directory_html}/{output_html}.html"
    else:
        output_path = f"{output_html}.html"

    try:
        with open(output_path, "w") as file:
            file.write(dataset_links)
        print(
            f"\nWrote HTML file with dataset links meeting "
            f"filter conditions: {output_path}"
        )
    except (IOError, OSError):
        print(f"ERROR: Could not write to file {output_path}")
    except (FileNotFoundError, PermissionError, OSError):
        print(f"ERROR: Could not open file {output_path}")

    return dataset_links


def main():
    """
    Returns list of package names that meet filter conditions.
    """
    args = parse_arguments()

    ckan = RemoteCKAN(address=args.ckan_address, apikey=args.authorization)
    ckan_pkgs = ckan_current_package_list_with_resources(ckan)

    if args.filter > 3:
        pkg_names = [filter_conditions_v2(pkg, args.filter) for pkg in ckan_pkgs]
    else:
        pkg_names = [filter_conditions(pkg, args.filter) for pkg in ckan_pkgs]

    pkgs_filtered = list(filter(lambda item: item is not None, pkg_names))

    if args.write_html:
        if not pkgs_filtered:
            print(
                "\n0 packages found that meet filter conditions, "
                "cannot write HTML file"
            )
        else:
            write_html_dataset_links(
                pkgs_filtered, args.ckan_address, args.filter, args.directory_html
            )

    print(f"\nNumber of packages that match filter conditions: {len(pkgs_filtered)}")
    print("\nNames of packages that match filter conditions:")
    return pkgs_filtered


if __name__ == "__main__":
    pprint(main())
    print("\n")
