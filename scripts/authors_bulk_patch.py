"""Script that parses a CSV file with author information for one dataset. Used to
patch a large group of authors for a particular dataset.

Note: this script will REPLACE all existing authors!

See get_author_format() to see supported input CSV formats.

# Example command to run script from root directory:
    python -m scripts.authors_bulk_patch --format 1 --csv "<input_csv_path>"
    --package test16 --authorization <nice envidat cookie>
"""

import argparse
import csv
import json

from ckanapi import RemoteCKAN


def parse_arguments():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-f",
        "--format",
        choices=[1],
        type=int,
        required=True,
        help="CSV file author format to parse. See function get_author_format() for "
        "supported formats.",
    )

    parser.add_argument(
        "-c",
        "--csv",
        required=True,
        help="Path to CSV file with author information for one dataset.",
    )

    parser.add_argument(
        "-p",
        "--package",
        required=True,
        help="Name or ID of EnviDat CKAN package (dataset) that will have authors "
        "patched.",
    )

    parser.add_argument(
        "-a",
        "--authorization",
        required=True,
        help="EnviDat CKAN cookie for logged in user passed in authorization header.",
    )

    parser.add_argument(
        "-k",
        "--ckan_address",
        default="https://www.envidat.ch",
        help="CKAN API host address. Default value is 'https://www.envidat.ch'",
    )

    return parser.parse_args()


def get_author_format(format_option: int) -> dict:
    """Return dictionary that maps EnviDat CKAN author dictionary format to a supported
    format option that corresponds to the header of an input CSV file.

    Args:
        format_option: supported format option found in input CSV file
    """
    match format_option:
        case 1:
            return {
                "affiliation": "Main affiliation (Department, Institute name, City, "
                "Country):",
                "email": "Email",
                "given_name": "First name (plus potential middle name or initials) to "
                "be used in the paper",
                "identifier": "ORCID",
                "name": "Last name to be used in the paper",
                "data_credit": "Contribution",
            }
        case _:
            raise ValueError(f"Unsupported format option: {format_option}")


def convert_author_to_envidat(
    csv_author: str, format_author: dict, format_option: int
) -> list[dict]:
    """Return list of dictionaries in EnviDat CKAN author format converted from input CSV
    file. Every author has his/her own dictionary.

    Args:
        csv_author: path to input CSV file with author information
        format_author: dictionary that maps EnviDat CKAN author format to the header
            of the input CSV file
        format_option: supported format option found in input CSV file
    """
    with open(csv_author, encoding="utf-8-sig") as author_csv_file:
        reader = csv.DictReader(author_csv_file)
        authors = []

        for row in reader:
            author = {}
            for key, value in format_author.items():
                if format_option == 1 and key == "data_credit":
                    author[key] = [
                        item.strip().lower() for item in row[value].split(",")
                    ]
                else:
                    author[key] = (row[value]).strip()
            authors.append(author)

    return authors


def main():
    """Main entry point for this script."""
    args = parse_arguments()

    format_dict = get_author_format(args.format)
    authors = convert_author_to_envidat(args.csv, format_dict, args.format)
    patch_fields = {"author": json.dumps(authors, ensure_ascii=False)}

    ckan = RemoteCKAN(address=args.ckan_address, apikey=args.authorization)
    response = ckan.action.package_patch(id=args.package, **patch_fields)

    return response


if __name__ == "__main__":
    print(main())
