"""Router used to convert EnviDat CKAN packages to external formats."""

import logging
from typing import Annotated, List

from fastapi import APIRouter, Query, HTTPException, Request, Security
from fastapi.security import APIKeyHeader
from fastapi.responses import StreamingResponse

from envidat.converters.bibtex_converter import convert_bibtex
from envidat.converters.datacite_converter import convert_datacite
from envidat.converters.dcat_ap_converter import convert_dcat_ap
from envidat.converters.dif_converter import convert_dif
from envidat.converters.iso_converter import convert_iso
from envidat.converters.ris_converter import convert_ris

from app.converters.constants import Converter
from app.converters.responses import (
    stream_response_attachment,
    open_ckan_current_package_list_with_resources,
    get_converter_extension,
    format_websnap_config,
    get_current_week_package_list,
)
from app.remote_ckan import (
    ckan_package_show,
    get_ckan,
    get_ckan_public,
    ckan_package_list,
    ckan_current_package_list_with_resources,
)

log = logging.getLogger(__name__)

router = APIRouter(prefix="/internal-dataset", tags=["internal-dataset"])

authorization_header = APIKeyHeader(
    name="Authorization",
    description="CKAN cookie for logged in user passed in authorization header",
    auto_error=False,
)


@router.get(
    "/convert/{converter}",
    name="Convert EnviDat dataset to other metadata format",
    status_code=200,
    responses={
        200: {
            "description": "EnviDat CKAN dataset (package) successfully converted to "
            "EnviDat package format. Converted format opens as a "
            "streamable download file."
        }
    },
)
def convert_envidat_package(
    request: Request,
    converter: Converter,
    package_id: Annotated[
        str,
        Query(
            alias="package-id",
            description="EnviDat CKAN package id or name",
            openapi_examples={
                "package name": {
                    "summary": "EnviDat CKAN package name",
                    "value": "data-reliability-study-avalanche-size-and-outlines",
                },
                "package ID": {
                    "summary": "EnviDat CKAN package ID",
                    "value": "64c2ac8a-5ab9-41bf-89b7-b838d3725966",
                },
            },
        ),
    ],
    auth: str = Security(authorization_header),
) -> StreamingResponse:
    """
    Convert EnviDat CKAN package to another specified metadata format
    and return StreamingResponse as downloadable attachment.

    If authorization header passed then converts datasets available only authorized
    users such as draft datasets owned by the user.
    """

    # Check if auth passed in Swagger UI
    if auth:
        authorization = auth
    # Check if authorization passed in header, else assign authorization to None
    else:
        authorization = request.headers.get("Authorization", None)

    # Get CKAN session
    # If fails to get CKAN session then raises HTTPException
    try:
        # Get privately accessible CKAN API session
        if authorization:
            ckan = get_ckan(authorization)
        # Get publicly accessible CKAN API session
        else:
            ckan = get_ckan_public()
    except Exception as e:
        log.debug(e)
        raise HTTPException(status_code=500, detail="Error, check logs") from e

    # Get package (if fails then raises HTTPException)
    package = ckan_package_show(package_id, ckan)

    package_name = package.get("name", "envidat_dataset")

    # Convert package to specified format
    try:
        match converter:
            case Converter.BIBTEX:
                bibtex = convert_bibtex(package)
                return stream_response_attachment(bibtex, f"{package_name}_bibtex.bib")

            case Converter.DATACITE:
                name_doi_map = {
                    package.get("name", "Unknown"): package.get("doi", "Unknown")
                }
                datacite = convert_datacite(package)
                return stream_response_attachment(
                    datacite, f"{package_name}_datacite.xml"
                )

            case Converter.DCAT_AP:
                dcat_ap = convert_dcat_ap(package)
                return stream_response_attachment(
                    dcat_ap, f"{package_name}_dcat-ap.xml"
                )

            case Converter.DIF:
                dif = convert_dif(package)
                return stream_response_attachment(dif, f"{package_name}_gcmd_dif.xml")

            case Converter.ISO:
                iso = convert_iso(package)
                return stream_response_attachment(iso, f"{package_name}_iso19139.xml")

            case Converter.RIS:
                ris = convert_ris(package)
                return stream_response_attachment(ris, f"{package_name}_ris.ris")

            # Default case, raises HTTP exception
            case _:
                raise HTTPException(
                    status_code=422,
                    detail="Value for 'converter' path parameter cannot be processed",
                )

    except Exception as e:
        log.error(e)
        raise HTTPException(
            status_code=500, detail="Failed to convert internal dataset"
        )


@router.get(
    "/convert-all/opendata-swiss",
    name="Convert all EnviDat datasets to DCAT-AP-CH format used by opendata.swiss",
    status_code=200,
    responses={
        200: {
            "description": "All EnviDat CKAN datasets (packages) successfully converted"
            " to DCAT-AP-CH format used by opendata.swiss. Converted format opens as a"
            " streamable download file."
        }
    },
)
def convert_all_envidat_packages_to_opendata_swiss() -> StreamingResponse:
    """
    Return all "open license" EnviDat CKAN datasets (packages) successfully converted to
    DCAT-AP-CH format used by opendata.swiss. Converted format opens as a
    streamable download file.

    Warning: This endpoint should only be used for websnap transfer purposes and not for
    frontend usage due to data processing time!
    """
    try:
        open_datasets = open_ckan_current_package_list_with_resources()
        dcat_ap = convert_dcat_ap(open_datasets)
        return stream_response_attachment(dcat_ap, "dcat-ap.xml")
    except Exception as e:
        log.error(e)
        raise HTTPException(
            status_code=500, detail="Failed to convert all opendata.swiss datasets"
        )


@router.get(
    "/websnap-config-all/{converter}",
    name="Create websnap config for all public EnviDat packages",
    status_code=200,
    responses={200: {"description": "Websnap compatible .ini configuration file."}},
)
def create_websnap_config(
    converter: Converter,
    bucket: str = Query(description="Name of S3 bucket in configuration file"),
    is_recent: bool = Query(
        False,
        alias="is-recent",
        description="If true then creates a websnap config only for EnviDat "
        "datasets that were modified within the last week",
    ),
    is_json: bool = Query(
        False,
        alias="is-json",
        description="If true then creates a .json formatted websnap config rather than "
        "the default .ini format",
    ),
    key_prefix: List[str] = Query(
        None,
        alias="key-prefix",
        description="Optional list of prefix items what will be prepended in order "
        "to each key in every config section",
    ),
) -> StreamingResponse:
    """
    Return websnap compatible .ini or .json configuration file for all public EnviDat
    packages.

    To learn more about how to create a websnap S3 config section please see
    https://gitlabext.wsl.ch/EnviDat/websnap#other-sections-one-per-api-url-endpoint

    Limitations:
    - Can only be used to create config sections for S3 type config sections
    - Can not be used to create the [DEFAULT] section of the websnap config,
        only creates config sections
    - Can only create a config for one type of converter, for example 'dif'
    - The bucket will be the same for all sections in the config (it is passed as a
        query parameter).
    - The package name is used as both the config section name and the key name
    """
    # Assign default error HTTP status_code
    error_status_code = 500

    try:
        if not (key_extension := get_converter_extension(converter)):
            log.error(f"Converter {converter} does not have matching file extension.")
            error_status_code = 422
            raise Exception("Value for 'converter' path parameter cannot be processed")

        ckan_session = get_ckan_public()

        if is_recent:
            current_pkgs = ckan_current_package_list_with_resources(ckan_session)
            package_list = get_current_week_package_list(current_pkgs)
        else:
            package_list = ckan_package_list(ckan_session)

        websnap_config = format_websnap_config(
            package_list, converter, bucket, key_extension, key_prefix, is_json
        )

        filename_extension = ".json" if is_json else ".ini"

        return stream_response_attachment(
            websnap_config, f"config_websnap{filename_extension}"
        )

    except Exception as e:
        log.error(e)
        raise HTTPException(
            status_code=error_status_code,
            detail="Failed to create websnap config, check logs",
        )
