"""Constants used in converters module"""

from enum import Enum


class Converter(str, Enum):
    """Names of converters available to convert EnviDat packages to external formats."""

    BIBTEX = "bibtex"
    DATACITE = "datacite"
    DCAT_AP = "dcat-ap"
    DIF = "dif"
    RIS = "ris"
    ISO = "iso"


class ConverterExtension(str, Enum):
    """
    File extensions for output files created by converters available to convert EnviDat
    packages to external formats.
    """

    BIBTEX = "bib"
    DATACITE = "xml"
    DCAT_AP = "xml"
    DIF = "xml"
    RIS = "ris"
    ISO = "xml"
