"""Module that formats and returns responses used by endpoints in api_converters.py"""

import json
import logging
from datetime import timedelta, datetime
from io import StringIO
from fastapi.responses import StreamingResponse

from app.converters.constants import Converter, ConverterExtension
from app.remote_ckan import get_ckan_public, ckan_call_action_handle_errors

log = logging.getLogger(__name__)


def stream_response_attachment(data: str, filename: str) -> StreamingResponse:
    """Convert data string to StringIO object and then return it as StreamingResponse
    attachment with specified filename.

    Args:
        data (str): data that will be streamed
        filename (str): name that be assigned to streamed file

    Returns:
        StreamingResponse: streams response as attachment with specified filename

    """
    stream = StringIO(data)
    headers = {"Content-Disposition": f"attachment; filename={filename}"}
    return StreamingResponse(stream, headers=headers)


def load_config(
    config_path: str = "app/converters/config/config_converters.json",
) -> dict | None:
    """Return config. If fails then returns None.

    Args:
        config_path (str): path to JSON config file
    """
    try:
        with open(config_path, "r") as conf:
            return json.load(conf)
    except FileNotFoundError as e:
        log.error(f"Cannot not find config at path {config_path}, error {e}")
        return None


def get_open_license_ids(config: dict) -> list[str] | None:
    """
    Return list of license_id values considered "openLicenses" in config.

    If "openLicenses" not found then returns None.
    """
    if open_license_ids := config.get("openLicenses"):
        return open_license_ids
    return None


def open_ckan_current_package_list_with_resources() -> list[dict]:
    """
    Return list of EnviDat CKAN package dictionaries (with resources and other
    metadata) that are considered "open".
    """
    config = load_config()
    if not config:
        raise FileNotFoundError("Cannot not find config")

    open_license_ids = get_open_license_ids(config)
    if not open_license_ids:
        raise AttributeError("Cannot find open license ids")

    ckan = get_ckan_public()
    packages = ckan_call_action_handle_errors(
        ckan, "current_package_list_with_resources", {"limit": "100000"}
    )

    open_pkgs = []
    for package in packages:
        if package["license_id"] in open_license_ids:
            open_pkgs.append(package)

    return open_pkgs


def get_current_week_package_list(current_packages: list[dict]) -> list[str]:
    """
    Return list of EnviDat CKAN package names that have metadata was modified within
    the last week.

    The algorithm breaks the loop iteration because it takes advantage of CKAN sorting
    most recently modified packages first, see
    https://docs.ckan.org/en/2.9/api/#ckan.logic.action.get.current_package_list_with_resources

    Args:
        current_packages: list of public EnviDat CKAN package dictionaries (with
            resources and other metadata)
    """
    recent_packages = []
    today = datetime.now()
    datetime_one_week_ago = today - timedelta(days=7)

    for pkg in current_packages:
        metadata_modified = pkg.get("metadata_modified")
        modified_date = datetime.fromisoformat(metadata_modified)
        if modified_date > datetime_one_week_ago:
            recent_packages.append(pkg.get("name"))
        else:
            break

    return recent_packages


def get_converter_extension(converter: Converter) -> ConverterExtension | None:
    """
    Return ConverterExtension that matches input converter.
    Converters are used to convert EnviDat packages to external formats.

    If converter cannot be matched then returns None.

    Args:
        converter (Converter): EnviDat package converter
    """
    match converter:
        case Converter.BIBTEX:
            return ConverterExtension.BIBTEX
        case Converter.DATACITE:
            return ConverterExtension.DATACITE
        case Converter.DCAT_AP:
            return ConverterExtension.DCAT_AP
        case Converter.DIF:
            return ConverterExtension.DIF
        case Converter.ISO:
            return ConverterExtension.ISO
        case Converter.RIS:
            return ConverterExtension.RIS
        case _:
            return None


def format_websnap_config(
    package_list: list[str],
    converter: Converter,
    bucket: str,
    key_extension: ConverterExtension,
    key_prefix: list[str] | None = None,
    is_json: bool = False,
) -> str:
    """
    Return .ini or .json formatted string with websnap compatible S3 config section for
    each package in package_list.

    Args:
        package_list (list[str]): list of public EnviDat package names
        converter: EnviDat package converter
        bucket (str): S3 bucket name
        key_extension: S3 key extension
        key_prefix: Optional list of prefix items what will be prepended in order
            to each key in every config section
        is_json: If true then creates a .json formatted websnap config rather than
                    the default .ini format
    """
    conf = load_config()
    if not conf:
        raise FileNotFoundError("Cannot not find config_converters.json")

    url_base = conf.get(
        "convertInternalDataSetAPI",
        "https://www.envidat.ch/converters-api/internal-dataset/convert",
    )

    if key_prefix:
        k_prefix = "/".join(key_prefix)
        k_prefix += "/"
    else:
        k_prefix = ""

    websnap_config = ""

    if is_json:
        conf_dict = {}
        for package in package_list:
            conf_dict[package] = {
                "url": f"{url_base}/{converter.value}?package-id={package}",
                "bucket": bucket,
                "key": f"{k_prefix}{package}.{key_extension.value}",
            }
        websnap_config = json.dumps(conf_dict)

    else:
        for package in package_list:
            websnap_config += (
                f"[{package}]\n"
                f"url={url_base}/{converter.value}?package-id={package}\n"
                f"bucket={bucket}\n"
                f"key={k_prefix}{package}.{key_extension.value}\n"
                f"\n"
            )

    return websnap_config
