"""Constants used in external_doi module"""

from enum import Enum
from typing import Any

from typing_extensions import TypedDict


class ConvertSuccess(TypedDict):
    """External platform conversion success class."""

    status_code: int
    result: dict


class ConvertError(TypedDict):
    """External platform conversion error class."""

    status_code: int
    message: str
    error: Any


class ExternalPlatform(str, Enum):
    """External platforms class."""

    ZENODO = "zenodo"
    DRYAD = "dryad"


EXTERNAL_PLATFORM_NAMES = {
    "zenodo": ExternalPlatform.ZENODO,
    "dryad": ExternalPlatform.DRYAD,
}

EXTERNAL_PLATFORM_PREFIXES = {"10.5281": ExternalPlatform.ZENODO}
