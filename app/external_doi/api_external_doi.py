"""Router used to convert DOIs and associated metadata from external platforms
into EnviDat CKAN package format.
"""

import logging
from typing import Annotated, List, Any, Dict
from pydantic import TypeAdapter, ValidationError

from fastapi import APIRouter, Query
from fastapi.responses import JSONResponse

from app.external_doi.constants import ConvertSuccess, ConvertError, ExternalPlatform
from app.external_doi.dryad import convert_dryad_doi
from app.external_doi.utils import get_doi_external_platform, convert_doi
from app.external_doi.zenodo import convert_zenodo_doi

log = logging.getLogger(__name__)

# Setup external-doi router
router = APIRouter(prefix="/external-doi", tags=["external-doi"])


@router.get(
    "/convert",
    name="Convert external DOI to EnviDat format",
    status_code=200,
    responses={
        200: {
            "model": Dict[str, Any],
            "description": "External DOI successfully converted to "
            "EnviDat package format",
        },
        400: {"model": ConvertError},
        404: {"model": ConvertError},
        500: {"model": ConvertError},
    },
)
async def convert_external_doi(
    doi: Annotated[
        str,
        Query(
            description="DOI from external platform",
            openapi_examples={
                "datacite": {
                    "summary": "DataCite DOI",
                    "value": "https://doi.org/10.13093/permos-2016-01",
                },
                "dryad": {
                    "summary": "Dryad DOI",
                    "value": "https://doi.org/10.5061/dryad.866t1g1v6",
                },
                "zenodo": {
                    "summary": "Zenodo DOI",
                    "value": "https://doi.org/10.5281/zenodo.6514932",
                },
            },
        ),
    ],
    owner_org: Annotated[
        str,
        Query(
            description="'owner_org' assigned to user in EnviDat CKAN",
            openapi_examples={
                "normal": {
                    "summary": "Example owner_org value",
                    "value": "bd536a0f-d6ac-400e-923c-9dd351cb05fa",
                }
            },
        ),
    ],
    tag: List[str] = Query(
        None,
        description="Optional tag list that will be assigned as tags to "
        "converted dataset",
    ),
    add_placeholders: Annotated[
        bool,
        Query(
            alias="add-placeholders",
            description="If true placeholder values are added for "
            "required EnviDat package fields",
        ),
    ] = False,
):
    """Convert metadata assigned to a DOI from external platforms
    into EnviDat CKAN package formatted json.

    Currently supports DOIs from DataCite, Dryad and Zenodo.
    """

    try:
        # Get external platform name and call corresponding API,
        # then convert the DOI's metadata to EnviDat CKAN package format.
        # If external platform not matched then external_platform is assigned None.
        external_platform = get_doi_external_platform(doi)
        match external_platform:
            case ExternalPlatform.ZENODO:
                result = convert_zenodo_doi(doi, owner_org, add_placeholders, tag)
            case ExternalPlatform.DRYAD:
                result = await convert_dryad_doi(doi, owner_org, add_placeholders, tag)
            case _:
                result = await convert_doi(doi, owner_org, add_placeholders, tag)

        # Extract status code from conversion result
        sc = result.get("status_code", 500)

        # Return result if conversion successful
        if is_type_valid(result, ConvertSuccess):
            cont = result.get("result", {})
            return JSONResponse(content=cont, status_code=sc)

        # Return error if conversion failed
        elif is_type_valid(result, ConvertError):
            return JSONResponse(content=result, status_code=sc)

        # Else raise exception if result cannot be validated as success or error
        else:
            raise Exception("Failed to validate result of conversion")

    except Exception as e:
        log.error(e)
        return JSONResponse(
            {
                "status_code": 500,
                "message": "Failed to convert external DOI",
                "error": "Check logs for error",
            }
        )


def is_type_valid(obj: Any, typ: Any) -> bool:
    """Return True if object is validated as type. Else return False.

    :param obj: Object to validate
    :param typ: Type used to validate object
    :return: boolean
    """
    try:
        validator = TypeAdapter(typ)
        validator.validate_python(obj)
        return True
    except ValidationError:
        return False
