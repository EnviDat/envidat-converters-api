"""Helper functions that format input data into EnviDat CKAN package format.
Used by multiple external DOI platform converters (zenodo, datacite, dryad, etc.)"""

import re


def get_name_envidat(title: str) -> str:
    """Returns name (of metadata entry) in lowercase with words joined by hyphens.

    If name with hyphens longer than 80 characters truncates name to last whole
    word.

    Args:
        title (str): title string in input record
    """
    regex_replacement = "[^0-9a-z- ]"
    name = re.sub(regex_replacement, "", title.lower())

    name_split = name.split(" ")
    name_join = "-".join(name_split)

    if len(name_join) > 80:
        name_trunc = name_join[:80]
        name_trunc_split = name_trunc.split("-")
        name_trunc_split.pop()
        return "-".join(name_trunc_split)

    return name_join


def get_maintainer(config: dict) -> dict:
    """Returns maintainer in EnviDat format.

    Args:
        config (dict): config dictionary
    """
    maintainer = {}

    name_default = (
        config.get("maintainer", {}).get("default", {}).get("name", "EnviDat")
    )

    email_default = (
        config.get("maintainer", {}).get("default", {}).get("email", "envidat@wsl.ch")
    )
    maintainer.update({"name": name_default, "email": email_default})

    return maintainer


def get_license(license_id: str, config: dict, add_placeholders: bool = False) -> dict:
    """Returns license data in dictionary with EnviDat formatted keys.

    Args:
        license_id (str): license_id string in input record
        config (dict): config dictionary
        add_placeholders (bool): If true placeholder values are added for
                     required EnviDat package fields. Default value is False.
    """
    # Extract licenses from config
    envidat_licenses = config.get("licenses", {})

    # Assign other_undefined for placeholder values and unknown licenses
    other_undefined = envidat_licenses.get(
        "other-undefined",
        {
            "license_id": "other-undefined",
            "license_title": "Other (Specified in the description)",
        },
    )

    if add_placeholders and not license_id:
        return other_undefined

    # Convert license_id to upper case for matching block and strip extra characters
    license_id = license_id.upper().strip()

    match license_id:
        case "CC-BY-4.0":
            return envidat_licenses.get("cc-by", other_undefined)

        case "CC-BY-SA-4.0":
            return envidat_licenses.get("cc-by-sa", other_undefined)

        case "CC-BY-NC-4.0":
            return envidat_licenses.get("cc-by-nc", other_undefined)

        case "CC0-1.0":
            return envidat_licenses.get("CC0-1.0", other_undefined)

        case _:
            return other_undefined


def get_tags(
    keywords: list, title: str, tags_query: list = None, add_placeholders: bool = False
) -> list:
    """Return tags in EnviDat format.

    Args:
        keywords (list): keywords in input record
        title (str): title string in input record
        tags_query (list): List of strings passed as query parameters that
                   should be added as tags. Default value is None.
        add_placeholders (bool): If true placeholder values are added for
                   required EnviDat package fields. Default value is False.
    """
    tags = []

    # Handle all keywords in one element and separated by commas or semicolons
    if len(keywords) == 1:
        keywords = keywords[0].replace(";", ",").split(",")

    # Format keywords
    regex_replacement = "[^0-9A-Z-_. ]"
    for keyword in keywords:
        word = re.sub(regex_replacement, "", keyword.upper().strip())
        if len(word) < 101:
            tags.append({"name": word})

    # Format tags_query if they exist
    if tags_query:
        tag_names = [item.get("name") for item in tags]

        for tag in tags_query:
            word = re.sub(regex_replacement, "", tag.upper().strip())
            if word not in tag_names and len(word) < 101:
                tags.append({"name": word})

    if add_placeholders:
        extra_tags = get_extra_tags(title, tags)
        tags += extra_tags

    return tags


def get_extra_tags(title: str, tags: list) -> list:
    """Returns extra tags extracted from title in EnviDat format.

    Function used to generate extra tags because at least 5 tags are required to create
    an EnvDat CKAN package. Duplicate tags are allowed to be sent to CKAN but will not
    appear more than once in CKAN package.

    Args:
       title (str): title string in input record
       tags (list): tags list in EnviDat format
    """
    extra_tags = []

    tag_values = []
    for tag in tags:
        tag_values.append(tag.get("name"))

    if len(tags) < 5:
        num_new_tags = 5 - len(tags)
        counter = 0

        words = title.split(" ")
        index = 0

        while counter <= num_new_tags:
            # Handle short titles
            if index + 1 > len(words):
                extra_tags.append({"name": f"DATA_{index}"})
                counter += 1
                index += 1
                continue

            # Format word
            regex_replacement = "[^0-9A-Z-_. ]"
            word = re.sub(regex_replacement, "", words[index].upper().strip())

            # Append formatted word to extra_tags if it is at least 4 characters
            if (
                4 <= len(word) < 101
                and word not in extra_tags
                and word not in tag_values
            ):
                extra_tags.append({"name": word})
                counter += 1

            index += 1

    return extra_tags
