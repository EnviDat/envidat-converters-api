"""Retrieve and convert Zenodo DOI metadata to EnviDat CKAN package format."""

import json
import logging
from datetime import date, datetime

import markdownify
import requests
import xlsxwriter

from app.external_doi.config.load_config import load_config
from app.external_doi.constants import ConvertError, ConvertSuccess
from app.external_doi.formatters_envidat import (
    get_name_envidat,
    get_maintainer,
    get_license,
    get_tags,
)
from app.remote_ckan import ckan_current_package_list_with_resources, get_ckan

log = logging.getLogger(__name__)


def convert_zenodo_doi(
    doi: str,
    owner_org: str,
    add_placeholders: bool = False,
    tag: list[str] | None = None,
) -> ConvertSuccess | ConvertError:
    """Return metadata for input doi if it exists in Zenodo API
    and convert metadata to EnviDat CKAN package format.

    Note: Converts data that exists in Zenodo metadata record
    By default does not provide default placeholders values for
    EnviDat CKAN package required/optional fields.
    If add_placeholders is True adds default placeholder values.

    If Zenodo doi metadata cannot be retrieved or conversion fails then
    returns error dictionary.

    Args:
        doi (str): Input doi string
        owner_org (str): 'owner_org' assigned to user in EnviDat CKAN
        add_placeholders (bool): If true placeholder values are added for
                       required EnviDat package fields. Default value is False.
        tag (list or None): Optional tag list that will be assigned as tags to
            converted dataset, default value is None.

    Returns:
        dict: Dictionary with metadata in EnviDat CKAN package
                or error dictionary
    """
    # Extract record_id
    if not (record_id := get_zenodo_record_id(doi)):
        return {
            "status_code": 400,
            "message": f"The following DOI was not found: {doi}",
            "error": f"Cannot extract record ID from input Zenodo DOI: {doi}",
        }

    # Load config
    config = load_config()
    if not config:
        return {
            "status_code": 500,
            "message": "Cannot process DOI. Please contact EnviDat team.",
            "error": "Cannot find config file",
        }

    # Assign variables needed to retrieve record from Zenodo API
    records_url = config.get("zenodoAPI", {}).get(
        "zenodoRecords", "https://zenodo.org/api/records"
    )
    api_url = f"{records_url}/{record_id}"
    timeout = config.get("timeout", 3)

    try:
        # Get record from Zenodo API
        log.debug(f"Calling Zenodo API with URL: {api_url}")
        response_zenodo = requests.get(api_url, timeout=timeout)

        # Handle unsuccessful response
        if response_zenodo.status_code != 200:
            return {
                "status_code": response_zenodo.status_code,
                "message": f"The following DOI was not found: {doi}",
                "error": response_zenodo.json(),
            }

        # Convert Zenodo record to EnviDat format
        envidat_record = convert_zenodo_to_envidat(
            response_zenodo.json(), owner_org, config, add_placeholders, tag
        )

    except Exception as e:
        log.error(f"ERROR: {e}")
        return {
            "status_code": 500,
            "message": f"Could not process DOI {doi}. Please contact EnviDat team.",
            "error": f"Failed to process DOI {doi}. Check logs for errors.",
        }

    return {"status_code": 200, "result": envidat_record}


def get_zenodo_record_id(doi: str) -> str | None:
    """Return record ID extracted from Zenodo Doi.
    If extraction fails return None.

    Example DOI: "10.5281/zenodo.5230562"
    Example returns record ID: "5230562"

    Args:
        doi (str): Input doi string

    Returns:
        str | None: String with record ID or None

    """
    if (period_index := doi.rfind(".")) == -1:
        return None

    if not (record_id := doi[period_index + 1 :]):
        return None

    return record_id.strip()


def convert_zenodo_to_envidat(
    data: dict,
    owner_org: str,
    config: dict,
    add_placeholders: bool = False,
    tag: list[str] | None = None,
) -> dict:
    """Convert Zenodo record dictionary to EnviDat CKAN package format.

    If add_placeholders true then add default values from config.
    Values added are required by EnviDat CKAN to create a new package.

    Args:
        data (dict): Response data object from Zenodo API call
        owner_org (str): 'owner_org' assigned to user in EnviDat CKAN
        config (dict): config dictionary created from config/config_external_doi.json
        add_placeholders (bool): If true placeholder values are added for
                       required EnviDat package fields. Default value is False.
        tag (list or None): Optional tag list that will be assigned as tags to
            converted dataset, default value is None.

    Returns:
        Dictionary in EnviDat CKAN package format.
    """
    # Assign dictionary to contain values converted from Zenodo
    # to EnviDat CKAN package format
    pkg = {}

    # Extract "metadata" dictionary from input "data"
    # metadata is used to extract and convert values to EnviDat package format
    metadata = data.get("metadata", {})

    # doi
    pkg.update({"doi": (doi := data.get("doi"))})

    # title
    # Return error if title not found
    if not (title := metadata.get("title")):
        err = f"'title' not found in DOI: {doi}"
        log.error(err)
        return {
            "status_code": 500,
            "message": f"Could not process DOI: {doi}",
            "error": err,
        }
    # Else add title to pkg
    pkg.update({"title": title})

    # name
    if name := get_name_envidat(title):
        pkg.update({"name": name})

    # author
    creators = metadata.get("creators", [])
    if authors := get_authors(creators, config, add_placeholders):
        pkg.update({"author": json.dumps(authors, ensure_ascii=False)})

    # maintainer
    maintainer = get_maintainer(config)
    pkg.update({"maintainer": json.dumps(maintainer, ensure_ascii=False)})

    # owner_org
    pkg.update({"owner_org": owner_org})

    # date
    publication_date = metadata.get("publication_date", "")
    if dte := get_date(publication_date, add_placeholders):
        pkg.update({"date": json.dumps(dte, ensure_ascii=False)})

    # private (True is default setting, this causes datasets to be "Unpublished")
    pkg.update({"private": True})

    # publication
    if publication := get_publication(publication_date, config, add_placeholders):
        pkg.update({"publication": json.dumps(publication, ensure_ascii=False)})

    # funding
    grants = metadata.get("grants", [])
    if funding := get_funding(grants, config, add_placeholders):
        pkg.update({"funding": json.dumps(funding, ensure_ascii=False)})

    # language ("en" English is default language)
    pkg.update({"language": "en"})

    # license
    license_id = metadata.get("license", {}).get("id", "")
    license_data = get_license(license_id, config, add_placeholders)

    pkg.update(
        {
            "license_id": license_data.get("license_id", "other-undefined"),
            "license_title": license_data.get(
                "license_title", "Other (Specified in the description)"
            ),
        }
    )

    if license_url := license_data.get("license_url"):
        pkg.update({"license_url": license_url})

    # notes
    description = metadata.get("description", "")
    if notes := get_notes(description, config):
        pkg.update({"notes": notes})

    # related_publications
    references = metadata.get("references", [])
    if related_publications := get_related_publications(references):
        pkg.update({"related_publications": related_publications})

    # resource_type_general
    pkg.update({"resource_type_general": "dataset"})

    # spatial
    # default spatial value is point set to WSl Birmsensdorf, Switzerland
    # office coordinates
    if add_placeholders:
        spatial = config.get("spatial", {}).get(
            "default", '{"type": "Point", "coordinates": [8.4545978, 47.3606372]}'
        )
        pkg.update({"spatial": spatial})

    # version
    if version := metadata.get("version"):
        pkg.update({"version": version})

    # files
    files = data.get("files", [])
    if resources := get_resources(files):
        pkg.update({"resources": resources})

    # tags
    keywords = metadata.get("keywords", [])
    if tags := get_tags(keywords, title, tag, add_placeholders):
        pkg.update({"tags": tags})

    return pkg


def get_authors(creators: list, config: dict, add_placeholders: bool = False) -> list:
    """Returns authors in EnviDat formattted list.

    Args:
        creators (dict): creators list in Zenodo record
        config (dict): Zenodo config dictionary
        add_placeholders (bool): If true placeholder values are added for
                     required EnviDat package fields. Default value is False.
    """
    authors = []

    # Add empty object to creators to handle no creators
    # and add_placholders is true
    if add_placeholders and not creators:
        creators = [{}]

    for creator in creators:
        author = {}

        creator_names = creator.get("name", "")
        if creator_names and "," in creator_names:
            names = creator_names.partition(",")
            author.update({"given_name": names[2].strip(), "name": names[0].strip()})
        elif creator_names and " " in creator_names:
            names = creator_names.partition(" ")
            author.update({"given_name": names[0].strip(), "name": names[2].strip()})
        elif add_placeholders:
            name_default = (
                config.get("author", {}).get("default", {}).get("name", "UNKNOWN")
            )
            author.update({"name": name_default})

        affiliation = creator.get("affiliation", "")
        if affiliation:
            author.update({"affiliation": affiliation.strip()})
        elif add_placeholders:
            affiliation_default = (
                config.get("author", {})
                .get("default", {})
                .get("affiliation", "UNKNOWN")
            )
            author.update({"affiliation": affiliation_default})

        if identifier := creator.get("orcid", ""):
            author.update({"identifier": identifier.strip()})

        authors.append(author)

    return authors


def get_date(publication_date: str, add_placeholders: bool = False) -> list:
    """Returns dates in Envidat format.

    Args:
        publication_date (str): publication_date string in Zenodo record
        add_placeholders (bool): If true placeholder values are added for
                     required EnviDat package fields. Default value is False.
    """
    dates = []

    if add_placeholders and not publication_date:
        date_today = date.today()
        date_str = date_today.strftime("%Y-%m-%d")
        date_dict = {"date": date_str, "date_type": "created"}
        dates.append(date_dict)

    elif publication_date:
        date_dict = {"date": publication_date, "date_type": "created"}
        dates.append(date_dict)

    return dates


def get_publication(
    publication_date: str, config: dict, add_placeholders: bool = False
) -> dict:
    """Returns publication in EnviDat format.

    Args:
        publication_date (str): publication_date string in Zenodo record
        config (dict): Zenodo config dictionary
        add_placeholders (bool): If true placeholder values are added for
                     required EnviDat package fields. Default value is False.
    """
    publication = {}

    publisher_default = (
        config.get("publication", {}).get("zenodo", {}).get("publisher", "Zenodo")
    )

    if add_placeholders and not publication_date:
        date_today = date.today()
        year = date_today.strftime("%Y")
        publication.update({"publication_year": year, "publisher": publisher_default})

    elif publication_date:
        try:
            dt = datetime.strptime(publication_date, "%Y-%m-%d")
            year = str(dt.year)
            publication.update(
                {"publication_year": year, "publisher": publisher_default}
            )
        except ValueError:
            date_today = date.today()
            year = date_today.strftime("%Y")
            publication.update(
                {"publication_year": year, "publisher": publisher_default}
            )
            return publication

    return publication


def get_funding(grants: list, config: dict, add_placeholders: bool = False) -> list:
    """Returns funding in EnviDat formatted list.

    Args:
        grants (list): grants list in Zenodo record
        config (dict): Zenodo config dictionary
        add_placeholders (bool): If true placeholder values are added for
                     required EnviDat package fields. Default value is False.
    """
    funding = []

    if add_placeholders and not grants:
        funder_default = (
            config.get("funding", {}).get("default", {}).get("institution", "UNKNOWN")
        )
        funder = {"institution": funder_default}
        funding.append(funder)

    for grant in grants:
        institution = grant.get("funder", {}).get("name")
        funding.append({"institution": institution})

    # Remove duplicate funders
    funding_no_duplicates = []
    for funder in funding:
        if funder not in funding_no_duplicates:
            funding_no_duplicates.append(funder)

    return funding_no_duplicates


def get_notes(description: str, config: dict) -> str:
    """Returns notes, converts HTML to markdown,
    if notes are less than 100 characters then inserts
    message from config to beginning of notes.

    Args:
        description (str): description string in Zenodo record
        config (dict): Zenodo config dictionary
    """
    description_md = markdownify.markdownify(description.strip())

    notes_message = config.get("notes", {}).get(
        "default",
        "Automatic message from EnviDat Admin: the "
        "description of this dataset is too short and "
        "therefore, not informative enough. Please improve "
        "and then delete this message. ",
    )

    if len(description_md) < 100:
        description_md = f"{notes_message}{description_md}"

    return description_md.strip()


def get_related_publications(references: list) -> str:
    """Returns related_publications in markdown string (as an unordered list).

    If references empty then returns empty string ""

    Args:
        references (list): references list in Zenodo record
    """
    related_publications = ""

    if references:
        start_str = "* "
        related_publications = "\r\n * ".join(references)
        return f"{start_str}{related_publications}"

    return related_publications


def get_resources(files: list) -> list:
    """Return resource in EnviDat format.

    Args:
        files (list): files in Zenodo record
    """
    resources = []

    for file in files:
        resource = {}

        if name := file.get("key"):
            resource.update({"name": name})

        if url := file.get("links", {}).get("self"):
            resource.update({"url": url})

        if size := file.get("size"):
            resource.update({"size": size})

        if type_resource := file.get("type"):
            resource.update({"format": type_resource})

        restricted = {"level": "public", "allowed_users": "", "shared_secret": ""}
        resource.update({"restricted": json.dumps(restricted, ensure_ascii=False)})

        resources.append(resource)

    return resources


def get_envidat_dois(api_token: str) -> list[str]:
    """Returns a list of all DOIs in packages in an EnviDat CKAN instance.
    NOTE: if api token invalid will still return packages available in public API!

    Args:
        api_token (str): api token for CKAN
    """
    dois = []

    # Get all packages in EnviDat CKAN instance
    get_ckan(api_token)
    package_list = ckan_current_package_list_with_resources(api_token)

    # Extract doi from each package
    for package in package_list:
        doi = package.get("doi")
        if doi:
            dois.append(doi)

    return dois


def get_zenodo_dois(api_token: str, q: str, size: str = "10000") -> list[str] | None:
    """Return Zenodo DOIs extracted from records produced by search query.

    In case of errors returns None.
    NOTE: Only returns Zenodo DOIS that are NOT already in EnviDat CKAN instance.
    For Zenodo API documentation see: https://developers.zenodo.org/#records

    Args:
        api_token (str): api token for CKAN
        q (str): search query (using Elasticsearch query string syntax)
        size (str): number of results to return, default value is "10000"
    """
    # Get config
    config_path = "app/external_doi/config/config_external_doi.json"
    try:
        with open(config_path, "r") as zenodo_config:
            config = json.load(zenodo_config)
    except FileNotFoundError as e:
        log.error(f"{e}")
        return None

    # Assign records_url
    records_url = config.get("zenodoAPI", {}).get(
        "zenodoRecords", "https://zenodo.org/api/records"
    )

    # Get URL used to call Zenodo API
    if q:
        api_url = f"{records_url}/?q={q}&size={size}"
    else:
        api_url = f"{records_url}/?size={size}"

    log.info(f"Calling Zenodo records API with the URL: {api_url}")

    # Get response from Zenodo API and extract DOIs
    try:
        response = requests.get(api_url, timeout=10)

        # Handle unsuccessful response
        if response.status_code != 200:
            log.error(
                f"Could not return Zenodo records " f"for the following URL: {api_url}"
            )
            return None

        # Extract records from Zenodo response
        response_json = response.json()
        records = response_json.get("hits", {}).get("hits", [])

        # Get EnviDat dois
        envidat_dois = get_envidat_dois(api_token)

        # Get list of Zenodo DOIs not already in EnviDat and that contain 'zenodo'
        dois = []
        for record in records:
            doi = record.get("doi")
            if doi:
                if doi in envidat_dois:
                    log.info(f"DOI already in EnviDat: {doi}")
                elif "zenodo" in doi:
                    dois.append(doi)

        return dois

    except Exception as e:
        log.error(f"{e}")
        return None


def write_dois_urls(
    dois: list[str], doi_prefix: str = None, output_path: str = "zenodo_dois.xls"
):
    """Writes list of DOIs to Excel file.
    Each DOI will be a clickable URL in the output Excel file.

    Args:
        dois (list): list of DOI strings
        doi_prefix (str): if doi_prefix included then prepends doi_prefix to each DOI
        output_path (str): path and name of output file, if specified then writes file
         there, else writes to root directory with default name "zenodo_dois.xlsx"
    """
    if doi_prefix:
        dois = [f"{doi_prefix}{doi}" for doi in dois]

    workbook = xlsxwriter.Workbook(output_path)
    worksheet = workbook.add_worksheet()

    row = 0
    column = 0

    for doi in dois:
        worksheet.write_url(row, column, doi)
        row += 1

    workbook.close()

    return
