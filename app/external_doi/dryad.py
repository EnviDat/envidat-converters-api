"""Retrieve and convert Dryad DOI metadata to EnviDat CKAN package format"""

import asyncio
import json
from urllib.parse import quote_plus
import pandas as pd
import geopandas
from shapely import geometry as geometry
import markdownify
import requests
from datetime import date, datetime

from app.external_doi.config.load_config import load_config
from app.external_doi.constants import ConvertSuccess, ConvertError
from app.external_doi.formatters_envidat import (
    get_name_envidat,
    get_maintainer,
    get_license,
    get_tags,
)

import logging

log = logging.getLogger(__name__)


async def convert_dryad_doi(
    doi: str,
    owner_org: str,
    add_placeholders: bool = False,
    tag: list[str] | None = None,
) -> ConvertSuccess | ConvertError | dict[str, dict | BaseException | int]:
    """Return metadata for input doi if it exists in Dryad API
    and convert metadata to EnviDat CKAN package format.

    Note: Converts data that exists in Dryad metadata record
    By default does not provide default placeholders values for
    EnviDat CKAN package required/optional fields.
    If add_placeholders is True adds default placeholder values.

    If Dryad DOI metadata cannot be retrieved or conversion fails then
    returns error dictionary.

    For Dryad API reference see https://datadryad.org/stash/api and
        https://datadryad.org/stash/api#/datasets/get_datasets__doi_

    :param doi: input doi string
    :param owner_org: 'owner_org' assigned to user in EnviDat CKAN
    :param add_placeholders: If true placeholder values are added for
                       required EnviDat package fields. Default value is False.
    :param tag: Optional tag list that will be assigned as tags to converted dataset,
                default value is None.
    :return: Dictionary with metadata in EnviDat CKAN package format
            or error dictionary.
    """

    # Extract DOI into format accepted by Dryad API
    if not (doi_dryad := format_doi(doi)):
        return {
            "status_code": 400,
            "message": f"Cannot extract DOI from input DOI string: {doi}",
            "error": f"Cannot extract DOI from input DOI string: {doi}",
        }

    # Load config
    config = load_config()
    if not config:
        return {
            "status_code": 500,
            "message": "Cannot process DOI. Please contact EnviDat team.",
            "error": "Cannot find config file",
        }

    # Assign variables needed to call retrieve a DOI (with metadata)
    # from Dryad API
    url_dryad = config.get("dryadAPI", {}).get(
        "dryadAPIDatasets", "https://datadryad.org/api/v2/datasets"
    )
    api_url = f"{url_dryad}/{doi_dryad}"
    timeout = config.get("timeout", 3)

    try:
        # Get DOI (with metadata) from Dryad API
        log.debug(f"Calling Dryad API with URL: {api_url}")
        response_dryad = requests.get(api_url, timeout=timeout)

        # Handle unsuccessful response
        if response_dryad.status_code != 200:
            return {
                "status_code": response_dryad.status_code,
                "message": f"The following DOI was not found: {doi}",
                "error": response_dryad.json(),
            }

        # Create task to convert Dryad record to EnviDat format
        convert_record = asyncio.create_task(
            convert_dryad_to_envidat(
                response_dryad.json(), owner_org, config, add_placeholders, tag
            )
        )

        # Create task to format Dryad record files metadata to
        # formatted "resources" (optional EnviDat field)
        convert_resources = asyncio.create_task(
            format_resources_dy(response_dryad.json(), config)
        )

        # Execute tasks and extract outputs
        conversions = await asyncio.gather(convert_record, convert_resources)
        envidat_record, envidat_resources = conversions

        # Merge formatted metadata and file metadata
        if envidat_resources:
            envidat_record.update({"resources": envidat_resources})

    except Exception as e:
        log.error(
            f"Error occured while trying to convert Dryad DOI to "
            f"EnviDat format: {e}"
        )
        return {
            "status_code": 500,
            "message": f"Could not process DOI {doi}, please contact EnviDat team.",
            "error": f"Failed to process DOI {doi}, check logs for errors.",
        }

    return {"status_code": 200, "result": envidat_record}


def format_doi(doi: str) -> str | None:
    """Return formatted probable DOI extracted from input doi string. DOI is URL
        encoded. If extraction fails return None.

    Example doi: https://doi.org/10.5061/dryad.866t1g1v6
    Example returns doi: "doi%3A10.5061%2Fdryad.866t1g1v6"

    :param doi: input doi string
    :return DOI as URL encoded string starting with "doi%3A" or None
    """
    if doi.startswith("doi:") or doi.startswith("doi%3A"):
        return quote_plus(doi.strip())

    elif doi.count("/") == 1:
        return quote_plus(f"doi:{doi.strip()}")

    elif (last_slash_index := doi.rfind("/")) == -1:
        return None

    elif (second_last_slash_index := doi.rfind("/", 0, last_slash_index)) == -1:
        return None

    else:
        doi_extract = doi[second_last_slash_index + 1 :].strip()
        return quote_plus(f"doi:{doi_extract}")


async def convert_dryad_to_envidat(
    data: dict,
    owner_org: str,
    config: dict,
    add_placeholders: bool = False,
    tag: list[str] | None = None,
) -> dict:
    """Convert Dryad DOI metadata dictionary to EnviDat CKAN package format.

    If add_placeholders true then add default values from config.
    Values added are required by EnviDat CKAN to create a new package.

    :param data: Response JSON content from Dryad record
    :param owner_org: owner_org assigned to user in EnviDat CKAN
    :param config: config dictionary created from config/config_external_doi.json
    :param add_placeholders: If true placeholder values are added for
                       required EnviDat package fields. Default value is False.
    :param tag: Optional tag list that will be assigned as tags
            to converted dataset, default value is None.
    :return: Dictionary in EnviDat CKAN package format.
    """
    # Assign dictionary to contain values converted from Dryad
    # to EnviDat CKAN package format
    pkg = {}

    # doi
    pkg.update({"doi": format_doi_dy(data.get("identifier"))})

    # title (needed because it used to create the required EnviDat "name" field)
    title = data.get("title")
    pkg.update({"title": title})

    # name (required EnviDat field)
    pkg.update({"name": get_name_envidat(title)})

    # author (required EnviDat field)
    authors = data.get("authors")
    pkg.update({"author": json.dumps(format_author_dy(authors), ensure_ascii=False)})

    # maintainer (required EnviDat field)
    maintainer = get_maintainer(config)
    pkg.update({"maintainer": json.dumps(maintainer, ensure_ascii=False)})

    # owner_org (required EnviDat field)
    pkg.update({"owner_org": owner_org})

    # date (required EnviDat field)
    publication_date = data.get("publicationDate", "")
    if date_list := format_date_dy(publication_date, add_placeholders):
        pkg.update({"date": json.dumps(date_list, ensure_ascii=False)})

    # publication (required EnviDat field)
    if publication := format_publication_dy(publication_date, add_placeholders):
        pkg.update({"publication": json.dumps(publication, ensure_ascii=False)})

    # funding (required EnviDat field)
    funders = data.get("funders", [])
    if funding := format_funding_dy(funders, config, add_placeholders):
        pkg.update({"funding": json.dumps(funding, ensure_ascii=False)})

    # language  (optional EnviDat field)
    # Set language to "en" (English)
    pkg.update({"language": "en"})

    # private (set to True, this causes datasets to be "Unpublished")
    pkg.update({"private": True})

    # license data (license_id is a required EnviDat field)
    license_dy = data.get("license", "")
    license_id = get_license_id_dy(license_dy, config, add_placeholders)
    license_data = get_license(license_id, config, add_placeholders)
    pkg.update(
        {
            "license_id": license_data.get("license_id", "other-undefined"),
            "license_title": license_data.get(
                "license_title", "Other (Specified in the description)"
            ),
        }
    )
    if license_url := license_data.get("license_url"):
        pkg.update({"license_url": license_url})

    # notes (required EnviDat field)
    abstract = data.get("abstract", "")
    usage_notes = data.get("usageNotes", "")
    if notes := format_notes_dy(abstract, usage_notes, config, add_placeholders):
        pkg.update({"notes": notes})

    # related_datasets (optional EnviDat field)
    # related_publications (optional EnviDat field)
    related_works = data.get("relatedWorks", [])
    related_datasets, related_publications = format_related_works_dy(related_works)
    if related_datasets:
        pkg.update({"related_datasets": related_datasets})
    if related_publications:
        pkg.update({"related_publications": related_publications})

    # resource_type_general  (required EnviDat field)
    pkg.update({"resource_type_general": "dataset"})

    # spatial  (required EnviDat field)
    # spatial_info  (optional EnviDat field)
    locations = data.get("locations", [])
    spatial, spatial_info = format_spatial_dy(locations, config, add_placeholders)
    if spatial:
        if isinstance(spatial, str):
            pkg.update({"spatial": spatial})
        else:
            pkg.update({"spatial": json.dumps(spatial, ensure_ascii=False)})
    if spatial_info:
        pkg.update({"spatial_info": spatial_info})

    # tags (required EnviDat field)
    keywords = data.get("keywords", [])
    if tags := get_tags(keywords, title, tag, add_placeholders):
        pkg.update({"tags": tags})

    # version (optional EnviDat field)
    if version := data.get("versionNumber", ""):
        pkg.update({"version": str(version)})

    return pkg


def format_doi_dy(identifier: str) -> str:
    """Return DOI in format used by EnviDat datasets.

    :param identifier: identifier string in Dryad record
    :return: Formatted DOI string
    """
    if identifier.startswith("doi:"):
        return identifier[4:]

    return identifier


def format_author_dy(authors: list) -> list[dict]:
    """Returns authors in EnviDat formatted list.

    :param authors: authors list in Dryad record
    :return: List of authors in EnviDat formatted dictionaries
    """
    ed_authors = []

    for author in authors:
        author_dict = {}

        author_dict.update(
            {
                "name": author.get("lastName"),
                "given_name": author.get("firstName"),
                "affiliation": author.get("affiliation"),
                "email": author.get("email"),
            }
        )

        if orcid := author.get("orcid"):
            author_dict.update({"identifier": orcid})

        ed_authors.append(author_dict)

    return ed_authors


def format_date_dy(publication_date: str, add_placeholders: bool = False) -> list[dict]:
    """Returns date in EnviDat formatted list ('date' is a list of date objects).

    If add_placeholders is True and publication_date is empty string "",
    then list with object with today's date with date_type 'created' is returned.

    :param add_placeholders: Boolean. If True placeholder values are added for
                     required EnviDat package fields. Default value is False.
    :param publication_date: publicationDate string in Dryad record
    :return: List of dates in EnviDat formatted dictionaries.
    """
    ed_dates = []

    if publication_date:
        date_dict = {}
        date_dict.update({"date": publication_date.strip(), "date_type": "created"})
        ed_dates.append(date_dict)

    if add_placeholders and not ed_dates:
        date_today = date.today()
        date_str = date_today.strftime("%Y-%m-%d")
        ed_dates.append({"date": date_str, "date_type": "created"})
        return ed_dates

    return ed_dates


def format_publication_dy(
    publication_date: str, add_placeholders: bool = False
) -> dict[str, str]:
    """Returns publication in EnviDat formatted dictionary.

    If add_placeholders is True and publication_date is empty string "",
    then dictionary with current year and publisher assigned as "EnviDat" returned.

    :param publication_date: publicationDate string in Dryad record
    :param add_placeholders: Boolean. If True placeholder values are added for
                     required EnviDat package fields. Default value is False.
    :return: Dictionary of publication information in EnviDat format.
    """
    publication = {}

    if publication_date:
        try:
            date_obj = datetime.strptime(publication_date, "%Y-%m-%d")
            publication.update(
                {"publication_year": str(date_obj.year), "publisher": "Dryad"}
            )
        except ValueError:
            log.debug(
                f"Datetime cannot be parsed from publication date: {publication_date}"
            )

    if add_placeholders and not publication:
        date_today = date.today()
        publication.update(
            {"publication_year": str(date_today.year), "publisher": "Dryad"}
        )

    return publication


def format_funding_dy(
    funders: list, config: dict, add_placeholders: bool = False
) -> list:
    """Returns funding in EnviDat formatted list.

    :param funders: funders list in Dryad record
    :param config: config dictionary
    :param add_placeholders: If true placeholder values are added for
                     required EnviDat package fields. Default value is False.
    :return: List of dictionaries with funding information in EnviDat format.
    """
    funding = []

    for ref in funders:
        ref_dict = {}

        if institution := ref.get("organization", ""):
            ref_dict.update({"institution": institution})

            if grant_number := ref.get("awardNumber", ""):
                ref_dict.update({"grant_number": grant_number})

        if ref_dict:
            funding.append(ref_dict)

    if add_placeholders and not funding:
        funder_default = (
            config.get("funding", {}).get("default", {}).get("institution", "UNKNOWN")
        )
        funder = {"institution": funder_default}
        funding.append(funder)

    return funding


def get_license_id_dy(
    license_dy: str, config: dict, add_placeholders: bool = False
) -> str:
    """Returns EnviDat license_id that corresponds to Dryad license.
    According to the Dryad documentation all datasets in Dryad should have a
    license value of https://creativecommons.org/publicdomain/zero/1.0/

    If add_placeholders is True and license is empty string "",
    then returns license_id value for config key "other-undefined".

    :param license_dy: license string in Dryad record
    :param config: config dictionary
    :param add_placeholders: Boolean. If True placeholder values are added for
                     required EnviDat package fields. Default value is False.
    :return: String that is EnviDat license_id
    """
    license_id = ""

    licenses = config.get("licenses", {})
    cc0_license = licenses.get("CC0-1.0", {})
    cc0_license_url = cc0_license.get(
        "license_url", "https://creativecommons.org/publicdomain/zero/1.0/"
    )

    if license_dy == cc0_license_url:
        license_id = cc0_license.get("license_id", "CC0-1.0")

    if add_placeholders and not license_id:
        license_id = licenses.get("other-undefined", {}).get(
            "license_id", "other-undefined"
        )

    return license_id


def format_notes_dy(
    abstract: str, usage_notes: str, config: dict, add_placeholders: bool = False
) -> str:
    """Returns notes in EnviDat formatted string. Converts HTML to markdown.

    If notes are less than 100 characters then inserts
    message from config to beginning of notes.

    :param abstract: abstract string in Dryad record
    :param usage_notes: usageNotes string in Dryad record
    :param config: config dictionary
    :param add_placeholders: Boolean. If True placeholder values are added for
                     required EnviDat package fields. Default value is False.
    :return: notes string
    """
    notes = f"{abstract} {usage_notes}"
    notes_md = markdownify.markdownify(notes.strip())

    if add_placeholders and len(notes_md) < 100:
        msg = config.get("notes", {}).get(
            "default",
            "Automatic message from EnviDat Admin: the "
            "description of this dataset is too short and "
            "therefore, not informative enough. Please improve "
            "and then delete this message. ",
        )
        notes_md = f"{msg}{notes_md}"

    return notes_md.strip()


def format_related_works_dy(related_works: list) -> (str, str):
    """Returns related_datasets and related_publications in EnviDat formatted strings.

    :param related_works: List of relatedWorks in Dryad record
    :return: Tuple with related_datasets string and related_publications string
    """
    related_datasets = ""
    related_publications = ""

    for item in related_works:
        identifier = item.get("identifier")

        if item.get("relationship") == "dataset":
            related_datasets += f"* {identifier}\r\n"
        else:
            related_publications += f"* {identifier}\r\n"

    return related_datasets, related_publications


def format_spatial_dy(
    locations: list, config: dict, add_placeholders: bool = False
) -> (dict, str):
    """Returns spatial data in EnviDat formatted dictionary and spatial_info as string.

    :param locations: locations list in Dryad record
    :param config: config dictionary
    :param add_placeholders: Boolean. If True placeholder values are added for
                     required EnviDat package fields. Default value is False.
    :return: Tuple with spatial dictionary and spatial_info string
    """
    spatial = {}
    spatial_info = ""
    geometries = []

    for location in locations:
        # Extract spatial_info
        if place := location.get("place"):
            spatial_info = place if not spatial_info else f"{spatial_info}, {place}"

        # Extract polygon
        if box := location.get("box"):
            polygon_coords = format_polygon_coords_dy(box)
            if len(locations) == 1 and polygon_coords:
                spatial.update({"type": "Polygon", "coordinates": [polygon_coords]})
            elif polygon_coords:
                geometries.append({"type": "Polygon", "coordinates": [polygon_coords]})

        # Extract point
        if point := location.get("point"):
            point_coords = format_point_coords_dy(point)
            if len(locations) == 1 and point_coords:
                spatial.update({"type": "Point", "coordinates": point_coords})
            elif point_coords:
                geometries.append({"type": "Point", "coordinates": point_coords})

    # Update spatial to include GeometryCollection (multiple items) spatial data
    if geometries:
        spatial.update({"type": "GeometryCollection", "geometries": geometries})

    # Default coordinates are WSL office location in Birmensdorf, Switzerland
    if add_placeholders and not spatial:
        spatial = config.get("spatial", {}).get(
            "default", '{"type": "Point", "coordinates": [8.4545978, 47.3606372]}'
        )

    return spatial, spatial_info


def format_point_coords_dy(point: dict) -> list | None:
    """Returns Dryad point dictionary as EnviDat formatted coordinate pair list.

    Output coordinates are converted to float and in [longitute, latitute] format.
    If conversion fails then returns None.

    Args:
        point (dict): point dictionary from Dryad "locations" record
    """
    longitude = point.get("longitude")
    latitude = point.get("latitude")

    if latitude and longitude:
        return [float(longitude), float(latitude)]

    return None


def format_polygon_coords_dy(box: dict) -> list | None:
    """Returns Dryad locations "box" dictionary as EnviDat formatted polygon
    coordinate pair list.

    Output coordinates are converted to float and in [longitute, latitute] format.
    If conversion fails then returns None.

    :param box: box dictionary from Dryad "locations" record
    :return: List (EnviDat format) or None
    """

    # Deserialize GeoJSON polygon to Python dictionary
    geo_json = box_to_polygon_dy(box)
    if geo_json:
        geo_dict = json.loads(geo_json)
    else:
        log.debug(f"Failed to convert locations box to GeoJSON polygon: {box}")
        return None

    # Extract coordinates
    crds = geo_dict.get("coordinates", [])
    if len(crds) == 1:
        coords = crds[0]
    else:
        log.debug(f"Unexpected coordinates list length for locations box: {box}")
        return None

    # Convert coordinates to EnviDat format
    coordinates = []
    for pair in coords:
        if len(pair) == 2:
            longitude = pair[0]
            latitude = pair[1]
            coordinates.append([float(longitude), float(latitude)])

    # Return coordinates if there are at least 4 coordinate pairs
    if len(coordinates) > 3:
        return coordinates

    # Else return None
    log.debug(f"Failed to convert coordinates from locations box: {box}")
    return None


def box_to_polygon_dy(box: dict) -> str | None:
    """Returns GeoJSON polygon extrapoloated from Dryad locations "box" dictionary.
    If conversion fails then returns None.

    :param box: box dictionary from Dryad "locations" record
    :return: String as GeoJSON Polygon or None
    """
    # Extract coordinates from box
    box_data = {
        "Latitude": [box.get("swLatitude"), box.get("neLatitude")],
        "Longitude": [box.get("swLongitude"), box.get("neLongitude")],
    }

    # Return None if any required coordinates are missing
    for val in box_data.values():
        if None in val:
            return None

    try:
        # Create data frames
        df = pd.DataFrame(box_data)
        gdf = geopandas.GeoDataFrame(
            df,
            geometry=geopandas.points_from_xy(df.Longitude, df.Latitude),
            crs="EPSG:4326",
        )

        # Get and return GeoJSON polygon
        bbox_tuple = gdf.total_bounds
        bbox_polygon = geometry.box(*bbox_tuple)
        geo_json = json.dumps(geometry.mapping(bbox_polygon))
        return geo_json

    except Exception as e:
        log.error(f"ERROR occured while converting locations box to polygon: {e}")
        return None


async def format_resources_dy(
    data: dict,
    config: dict,
) -> list[dict]:
    """Format resources (data files) in EnviDat format.

    :param data: Response JSON content from Dryad record
    :param config: config dictionary created from config/config_external_doi.json
    :return: List of resources in EnviDat format
    """

    resources = []
    dryad_host = config.get("dryadAPI", {}).get("dryadAPIHost", "https://datadryad.org")

    # Extract "stash:version" from data
    stash_version = data.get("_links", {}).get("stash:version", {}).get("href", "")

    # Get JSON response to call https://datadryad.org/api/v2/versions/{id}/files
    versions_files = get_versions_files_dy(stash_version, config)

    # Handle errors
    if not versions_files:
        return resources

    # Extract files list from versions_files
    files = versions_files.get("_embedded", {}).get("stash:files", [])

    # Format files in EnviDat resources format
    for file in files:
        file_obj = {}

        if (
            download_href := file.get("_links", {})
            .get("stash:file-download", {})
            .get("href", "")
        ):
            file_obj.update({"url": f"{dryad_host}{download_href}"})

            if path := file.get("path", ""):
                file_obj.update({"name": path})

        if file_obj:
            resources.append(file_obj)

    return resources


def get_versions_files_dy(
    stash_version: str,
    config: dict,
) -> dict | None:
    """Return response JSON content from Dryad record's files.
    If file metadata not found then returns None.

    :param stash_version: "stash:version" value in Dryad record
    :param config: config dictionary created from config/config_external_doi.json
    :return: Dictionary with Dryad files object or None.
    """

    # Assign variables needed to call retrieve a DOI (with metadata)
    # from Dryad API
    dryad_host = config.get("dryadAPI", {}).get("dryadAPIHost", "https://datadryad.org")
    api_url = f"{dryad_host}/{stash_version}/files"
    timeout = config.get("timeout", 3)

    try:
        # Get file metadata from DRYAD API
        log.debug(f"Calling Dryad API file metadata with URL: {api_url}")
        response_dryad = requests.get(api_url, timeout=timeout)

        # Handle unsuccessful response
        if response_dryad.status_code != 200:
            log.debug(
                f"Call to {api_url} returned HTTP status code: "
                f"{response_dryad.status_code}"
            )
            return None

        return response_dryad.json()

    except Exception as e:
        log.error(
            f"ERROR while trying to call Dryad API file metadata "
            f"with URL '{api_url}': {e}"
        )
        return None
