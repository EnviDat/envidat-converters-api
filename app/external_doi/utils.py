"""Utils for external_doi module."""

import csv
from logging import getLogger

import xlsxwriter

from app.external_doi.constants import (
    EXTERNAL_PLATFORM_NAMES,
    EXTERNAL_PLATFORM_PREFIXES,
    ConvertError,
    ConvertSuccess,
    ExternalPlatform,
)
from app.external_doi.datacite import convert_datacite_doi
from app.external_doi.dryad import convert_dryad_doi
from app.external_doi.zenodo import convert_zenodo_doi

log = getLogger(__name__)


def get_doi_external_platform(doi: str) -> ExternalPlatform | None:
    """Return ExternalPlatform that most likely corresponds to input DOI string.

    If external platform not found then returns None.

    Args:
        doi (str): Input DOI string

    Returns:
        ExternalPlatform | None
    """
    # Search by names for doi that corresponds to supported external platform
    for key, value in EXTERNAL_PLATFORM_NAMES.items():
        if key in doi:
            return value

    # Search by DOI prefixes for doi that corresponds to
    # supported external platforms
    for key, value in EXTERNAL_PLATFORM_PREFIXES.items():
        if key in doi:
            return value

    return None


async def convert_doi(
    doi: str,
    owner_org: str,
    add_placeholders: bool = False,
    tag: list[str] | None = None,
) -> ConvertSuccess | ConvertError:
    """Tries to return metadata for input DOI and convert metadata to EnviDat
    CKAN package format.

    Calls supported external platforms. If DOI cannot be matched to external platform
    then returns error dictionary.

    Args:
        doi (str): Input DOI string
        owner_org (str): 'owner_org' assigned to user in EnviDat CKAN
        add_placeholders (bool): If true placeholder values are added for
                       required EnviDat package fields. Default value is False.
        tag (list[str] | None): Optional tag list that will be assigned as tags to
                        converted dataset for some converters. Default value is None.
    """
    converters = [convert_datacite_doi, convert_dryad_doi, convert_zenodo_doi]

    for converter in converters:
        if converter is convert_datacite_doi:
            record = convert_datacite_doi(doi, owner_org, add_placeholders, tag)
            if record.get("status_code") == 200:
                return record
            else:
                log.debug(f"DataCite conversion failed for DOI '{doi}': {record}")

        if converter is convert_dryad_doi:
            record = await convert_dryad_doi(doi, owner_org, add_placeholders, tag)
            if record.get("status_code") == 200:
                return record
            else:
                log.debug(f"Dryad conversion failed for DOI '{doi}': {record}")

        if converter is convert_zenodo_doi:
            record = convert_zenodo_doi(doi, owner_org, add_placeholders)
            if record.get("status_code") == 200:
                return record
            else:
                log.debug(f"Zenodo conversion failed for DOI '{doi}': {record}")

    return {
        "status_code": 404,
        "message": f"The following DOI is not currently "
        f"supported for conversion: {doi}",
        "error": f"Cannot convert the DOI: {doi}",
    }


def write_dois_urls(
    dois: list[str], doi_prefix: str = None, output_path: str = "zenodo_dois.xls"
):
    """Writes list of DOIs to Excel file.
    Each DOI will be a clickable URL in the output Excel file.

    Args:
        dois (list): list of DOI strings
        doi_prefix (str): if doi_prefix included then prepends doi_prefix to each DOI
        output_path (str): path and name of output file, if specified then writes file
         there, else writes to root directory with default name "zenodo_dois.xlsx"
    """
    if doi_prefix:
        dois = [f"{doi_prefix}{doi}" for doi in dois]

    workbook = xlsxwriter.Workbook(output_path)
    worksheet = workbook.add_worksheet()

    row = 0
    column = 0

    for doi in dois:
        worksheet.write_url(row, column, doi)
        row += 1

    workbook.close()

    return


def read_dois_urls(input_path: str) -> list[str] | None:
    """Returns list of DOIs strings read from csv file.
    In case of errors returns None.

    Args:
        input_path (str): path and name of input file
    """
    try:
        with open(input_path, encoding="utf-8-sig") as file:
            reader = csv.reader(file)
            dois = []

            for row in reader:
                dois.append(row[0])

        return dois

    except Exception as e:
        log.error(f"{e}")
        return None
