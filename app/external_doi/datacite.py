"""Retrieve and convert DataCite DOI metadata to EnviDat CKAN package format"""

import json
import logging
from datetime import date

import requests

from app.external_doi.config.load_config import load_config
from app.external_doi.constants import ConvertSuccess, ConvertError
from app.external_doi.formatters_envidat import (
    get_name_envidat,
    get_maintainer,
    get_license,
    get_tags,
)

log = logging.getLogger(__name__)


def convert_datacite_doi(
    doi: str,
    owner_org: str,
    add_placeholders: bool = False,
    tag: list[str] | None = None,
) -> ConvertSuccess | ConvertError:
    """Return metadata for input doi if it exists in DataCite API
    and convert metadata to EnviDat CKAN package format.

    Note: Converts data that exists in DataCite metadata record
    By default does not provide default placeholders values for
    EnviDat CKAN package required/optional fields.
    If add_placeholders is True adds default placeholder values.

    If DataCite DOI metadata cannot be retrieved or conversion fails then
    returns error dictionary.

    For DataCite API reference see https://support.datacite.org/docs/api-get-doi and
        https://support.datacite.org/reference/get_dois-id

    :param doi: input doi string
    :param owner_org: 'owner_org' assigned to user in EnviDat CKAN
    :param add_placeholders: If true placeholder values are added for
                       required EnviDat package fields. Default value is False.
    :param tag: Optional tag list that will be assigned as tags to converted dataset,
                default value is None.
    :return: Dictionary with metadata in EnviDat CKAN package format
            or error dictionary.
    """

    # Extract DOI into format accepted by DataCite API
    if not (doi_dc := get_doi(doi)):
        return {
            "status_code": 400,
            "message": f"Cannot extract DOI from input DOI string: {doi}",
            "error": f"Cannot extract DOI from input DOI string: {doi}",
        }

    # Load config
    config = load_config()
    if not config:
        return {
            "status_code": 500,
            "message": "Cannot process DOI. Please contact EnviDat team.",
            "error": "Cannot find config file",
        }

    # Assign variables needed to call retrieve a DOI (with metadata)
    # from DataCite API URL
    url_dc = config.get("dataciteAPI", {}).get(
        "dataciteDOI", "https://zenodo.org/api/records"
    )
    api_url = f"{url_dc}/{doi_dc}"
    timeout = config.get("timeout", 5)

    try:
        # Get DOI (with metadata) from DataCite API
        log.debug(f"Calling Datacite API with URL: {api_url}")
        response_dc = requests.get(api_url, timeout=timeout)

        # Handle unsuccessful response
        if response_dc.status_code != 200:
            return {
                "status_code": response_dc.status_code,
                "message": f"The following DOI was not found: {doi}",
                "error": response_dc.json(),
            }

        # Convert DataCite record to EnviDat format
        envidat_record = convert_datacite_to_envidat(
            response_dc.json(), owner_org, config, add_placeholders, tag
        )

    except Exception as e:
        log.error(f"ERROR: {e}")
        return {
            "status_code": 500,
            "message": f"Could not process DOI {doi}, please contact EnviDat team.",
            "error": f"Failed to process DOI {doi}, check logs for errors.",
        }

    return {"status_code": 200, "result": envidat_record}


def get_doi(doi: str) -> str | None:
    """Return probable DOI extracted from input doi string.
    If extraction fails return None.

    Example doi: https://doi.org/10.13093/permos-2016-01
    Example returns doi: 10.13093/permos-2016-01

    :param doi: input doi string
    :return string with DOI or None
    """
    if doi.count("/") == 1:
        return doi.strip()

    if (last_slash_index := doi.rfind("/")) == -1:
        return None

    if (second_last_slash_index := doi.rfind("/", 0, last_slash_index)) == -1:
        return None

    return doi[second_last_slash_index + 1 :].strip()


def convert_datacite_to_envidat(
    data: dict,
    owner_org: str,
    config: dict,
    add_placeholders: bool = False,
    tag: list[str] | None = None,
) -> dict:
    """Convert DataCite DOI metadata dictionary to EnviDat CKAN package format.

    If add_placeholders true then add default values from config.
    Values added are required by EnviDat CKAN to create a new package.

    Args:
        data (dict): Response JSON content from DataCite API call
        owner_org (str): 'owner_org' assigned to user in EnviDat CKAN
        config (dict): config dictionary created from
                            config/config_external_doi.json
        add_placeholders (bool): If true placeholder values are added for
                       required EnviDat package fields. Default value is False.
        tag (list | None): Optional tag list that will be assigned as tags
            to converted dataset, default value is None.
    """
    # Assign dictionary to contain values converted from DataCite
    # to EnviDat CKAN package format
    pkg = {}

    # Extract metadata dictionary from input data "attributes" dictionary
    # attributes are used to extract and convert values to EnviDat package format
    metadata = data.get("data", {}).get("attributes", {})

    # doi
    pkg.update({"doi": (doi := metadata.get("doi"))})

    # title  (needed because it used to create the required EnviDat "name" field)
    # Return error if title not found
    if not (title := get_title_dc(metadata)):
        err = f"'title' not found in DOI: {doi}"
        log.error(err)
        return {
            "status_code": 500,
            "message": f"Could not process DOI: {doi}",
            "error": err,
        }
    # Else add title to pkg
    pkg.update({"title": title})

    # name  (required EnviDat field)
    if name := get_name_envidat(title):
        pkg.update({"name": name})

    # author  (required EnviDat field)
    creators = metadata.get("creators", [])
    if authors := get_authors_dc(creators, config, add_placeholders):
        pkg.update({"author": json.dumps(authors, ensure_ascii=False)})

    # maintainer  (required EnviDat field)
    maintainer = get_maintainer(config)
    pkg.update({"maintainer": json.dumps(maintainer, ensure_ascii=False)})

    # owner_org  (required EnviDat field)
    pkg.update({"owner_org": owner_org})

    # date  (required EnviDat field)
    dates = metadata.get("dates", [])
    if date_list := get_date_dc(dates, add_placeholders):
        pkg.update({"date": json.dumps(date_list, ensure_ascii=False)})

    # funding  (required EnviDat field)
    funding_references = metadata.get("fundingReferences", [])
    if funding := get_funding_dc(funding_references, config, add_placeholders):
        pkg.update({"funding": json.dumps(funding, ensure_ascii=False)})

    # language  (optional EnviDat field)
    # Default language is "en" (English)
    language = metadata.get("language", "en")
    pkg.update({"language": language})

    # private (set to True, this causes datasets to be "Unpublished")
    pkg.update({"private": True})

    # publication  (required EnviDat field)
    publication_year = str(metadata.get("publicationYear", ""))
    publisher = metadata.get("publisher", "")
    if publication := get_publication_dc(
        publication_year, publisher, config, add_placeholders
    ):
        pkg.update({"publication": json.dumps(publication, ensure_ascii=False)})

    # license data
    # license_id  (required EnviDat field)
    rights_list = metadata.get("rightsList", [])
    rights_identifier = ""

    # Use rightsIdentifier from first item in rightsList
    # (all other items in DataCite rightsList are ignored)
    if len(rights_list) > 0:
        rights_identifier = rights_list[0].get("rightsIdentifier", "")

    license_data = get_license(rights_identifier, config, add_placeholders)

    pkg.update(
        {
            "license_id": license_data.get("license_id", "other-undefined"),
            "license_title": license_data.get(
                "license_title", "Other (Specified in the description)"
            ),
        }
    )
    if license_url := license_data.get("license_url"):
        pkg.update({"license_url": license_url})

    # notes  (required EnviDat field)
    descriptions = metadata.get("descriptions", [])
    if notes := get_notes_dc(descriptions, config, add_placeholders):
        pkg.update({"notes": notes})

    # related_publications  (optional EnviDat field)
    # TODO in future handle "DOI" objects that do have an EnviDat prefix
    # TODO in future handle other "relatedIdentifierType" besides "DOI"
    related_identifiers = metadata.get("relatedIdentifiers", [])
    if related_publications := get_related_publications_dc(related_identifiers, config):
        pkg.update({"related_publications": related_publications})

    # resources  (optional EnviDat field)
    # Currently only extracts EnviDat resource URLs
    if resources_url := get_resources_url_dc(related_identifiers):
        pkg.update({"resources": resources_url})

    # Ignore DataCite "relatedItems" for now, values may potentially be assigned to
    # EnviDat field "related_publications" in future

    # resource_type_general  (required EnviDat field)
    pkg.update({"resource_type_general": "dataset"})

    # Ignored
    # sizes (optional EnviDat field)

    # Ignored
    # formats  (optional EnviDat field)

    # version  (optional EnviDat field)
    if version := metadata.get("version", ""):
        pkg.update({"version": str(version)})

    # spatial  (required EnviDat field)
    # spatial_info  (optional EnviDat field)
    geo_locations = metadata.get("geoLocations", [])
    spatial, spatial_info = get_spatial_dc(geo_locations)

    if spatial:
        pkg.update({"spatial": json.dumps(spatial, ensure_ascii=False)})
    elif len(spatial) == 0 and add_placeholders:
        spatial = config.get("spatial", {}).get(
            "default", '{"type": "Point", "coordinates": [8.4545978, 47.3606372]}'
        )
        pkg.update({"spatial": spatial})

    if spatial_info:
        pkg.update({"spatial_info": spatial_info})

    # tags  (required EnviDat field)
    subjects = metadata.get("subjects", [])
    subs = []

    for item in subjects:
        if subject := item.get("subject", ""):
            subs.append(subject)

    if tags := get_tags(subs, title, tag, add_placeholders):
        pkg.update({"tags": tags})

    return pkg


def get_authors_dc(
    creators: list, config: dict, add_placeholders: bool = False
) -> list:
    """Returns authors in EnviDat formatted list.

    Args:
        creators (list): creators list in DataCite record
        config (dict): config dictionary
        add_placeholders (bool): If true placeholder values are added for
                     required EnviDat package fields. Default value is False.

    """
    authors = []
    affiliation_default = (
        config.get("author", {}).get("default", {}).get("affiliation", "UNKNOWN")
    )

    if add_placeholders and not creators:
        name_default = (
            config.get("author", {}).get("default", {}).get("name", "UNKNOWN")
        )
        author = {}
        author.update({"name": name_default, "affiliation": affiliation_default})
        authors.append(author)

    for creator in creators:
        author = {}
        name = creator.get("name")
        given_name = creator.get("givenName")
        family_name = creator.get("familyName")
        nameType = creator.get("nameType")

        if nameType == "Organizational":
            author.update({"affiliation": name})
        elif add_placeholders:
            author.update({"affiliation": affiliation_default})

        if given_name and family_name:
            author.update({"name": family_name, "given_name": given_name})
        else:
            author.update({"name": name})

        authors.append(author)

    return authors


def get_date_dc(dates: list, add_placeholders: bool = False) -> list:
    """Returns date in EnviDat formatted list ('date' is a list of date objects)

    If add_placeholders is True and dates is empty or no dates can be parsed,
    then list with object with today's date with date_type 'created' is returned.

    Args:
        dates (list): dates list in DataCite record
        add_placeholders (bool): If True placeholder values are added for
                     required EnviDat package fields. Default value is False.
    """
    output_dates = []

    for dte in dates:
        date_obj = {}
        date_str = dte.get("date")
        date_type = dte.get("dateType")

        if date_str and date_type:
            date_obj.update(
                {"date": date_str.strip(), "date_type": date_type.lower().strip()}
            )
            output_dates.append(date_obj)

    if add_placeholders:
        if not dates or not output_dates:
            date_today = date.today()
            date_str = date_today.strftime("%Y-%m-%d")
            output_dates.append({"date": date_str, "date_type": "created"})
            return output_dates

    return output_dates


def get_title_dc(metadata) -> str | None:
    """Returns title extracted from first item DataCite DOI metadata titles list.

    If extraction fails then returns None.
    """

    titles = metadata.get("titles", [])

    if len(titles) > 0:
        if isinstance(titles[0], dict):
            return titles[0].get("title", "")
        else:
            return None
    else:
        return None


def get_publication_dc(
    publication_year: str, publisher: str, config: dict, add_placeholders: bool = False
) -> dict:
    """Returns publication in EnviDat format

    :param publication_year: Year DataCite record was published
    :param publisher: Publisher of DataCite Record
    :param config: Config dictionary
    :param add_placeholders: If true placeholder values are added for
                     required EnviDat package fields. Default value is False.
    :return: Dictionary of publication in EnviDat format
    """
    publication = {}

    if publication_year:
        publication.update({"publication_year": publication_year})

    if add_placeholders and not publication_year:
        date_today = date.today()
        year = date_today.strftime("%Y")
        publication.update({"publication_year": year})

    if publisher:
        publication.update({"publisher": publisher})

    if add_placeholders and not publisher:
        publisher_default = (
            config.get("publication", {})
            .get("datacite", {})
            .get("publisher", "UNKNOWN")
        )
        publication.update({"publisher": publisher_default})

    return publication


def get_funding_dc(
    funding_references: list, config: dict, add_placeholders: bool = False
) -> list:
    """Returns funding in EnviDat formatted list.

    Args:
        funding_references (list): fundingReferences list in DataCite record
        config (dict): config dictionary
        add_placeholders (bool): If true placeholder values are added for
                     required EnviDat package fields. Default value is False.
    """
    funding = []

    for ref in funding_references:
        institution = ref.get("funderName", "")
        grant_number = ref.get("awardNumber", "")

        if institution and grant_number:
            funding.append({"institution": institution, "grant_number": grant_number})
            continue

        if institution and not grant_number:
            funding.append({"institution": institution})

        if grant_number and not institution:
            funding.append({"grant_number": grant_number})

    if add_placeholders and not funding_references:
        funder_default = (
            config.get("funding", {}).get("default", {}).get("institution", "UNKNOWN")
        )
        funder = {"institution": funder_default}
        funding.append(funder)

    return funding


def get_notes_dc(
    descriptions: list, config: dict, add_placeholders: bool = False
) -> str:
    """Returns notes in EnviDat formatted string.

    If notes are less than 100 characters then inserts
    message from config to beginning of notes.

    Args:
        descriptions (list): descriptions list in DataCite record
        config (dict): config dictionary
        add_placeholders (bool): If true placeholder values are added for
                     required EnviDat package fields. Default value is False.
    """
    notes = ""

    notes_message = config.get("notes", {}).get(
        "default",
        "Automatic message from EnviDat Admin: the "
        "description of this dataset is too short and "
        "therefore, not informative enough. Please improve "
        "and then delete this message. ",
    )

    if add_placeholders and len(descriptions) == 1:
        description = descriptions[0].get("description", "")
        if len(description) < 100:
            notes = f"{notes_message}{description}"
            return notes

    for item in descriptions:
        description = item.get("description", "")
        description_type = item.get("descriptionType", "")

        # If description type is Abstract and other description items have already been
        # added to notes then insert description to beginning of notes
        if description_type == "Abstract" and len(notes) > 0:
            notes = f"{description}\n\n{notes}"
        elif len(notes) > 0:
            notes = f"{notes}\n\n{description}"
        else:
            notes = f"{description}"

    if add_placeholders and len(notes) < 100:
        notes = f"{notes_message}{notes}"

    return notes


# TODO check that Markdown imports correctly
def get_related_publications_dc(related_identifiers: list, config: dict) -> str:
    """Return related_publications as a string with DOI URLs
              (in Markdown unordered list format.)

      Format of DOI string: "https://www.doi.org/<DOI>
      Example DOI string: "https://www.doi.org/10.16904/envidat.395"

      NOTE: Currently only extracts DataCite "relatedIdentifiers" objects with
           "relatedIdentifierType" of "DOI",
           "relationType" is one of the list
           ["Cites", "IsSupplementTo", "IsSupplementedBy"],
           and that do NOT have an EnviDat prefix
           (or other prefixes specified in config).

    Args:
      related_identifiers (list): relatedIdentifiers list in DataCite record
      config (dict): config dictionary

    """
    excluded_prefixes = config.get("relatedDatasets", {}).get(
        "excludedPrefixes", ["10.16904"]
    )

    related_publications = ""

    for identifier in related_identifiers:
        if identifier.get("relatedIdentifierType") == "DOI" and identifier.get(
            "relationType"
        ) in ["Cites", "IsSupplementTo", "IsSupplementedBy"]:
            related_identifier = identifier.get("relatedIdentifier", "")

            for prefix in excluded_prefixes:
                if prefix not in related_identifier:
                    related_publications += (
                        f"* https://www.doi.org/{related_identifier}\r\n"
                    )

    return related_publications


def get_resources_url_dc(related_identifiers: list) -> list:
    """Returns resources_url as a list with dictionares with resource links.

    Format of dictionaries returned in list: {"url": <url>}
    Example of above format: {"url": "	"https://www.envidat.ch/dataset/f5f4a8fd-7172-4db0-9b41-832a94ea9cab/resource/3290357e-2e2f-4a10-a254-4689b5f91705/download/gbif.range.zip"}

    NOTE: Currently only accepts URLs that are known to be an EnviDat resource
    """
    resources_url = []

    for identifier in related_identifiers:
        if identifier.get("relatedIdentifierType") == "URL":
            related_identifier = identifier.get("relatedIdentifier", "")

            if (
                related_identifier.startswith("https://www.envidat.ch/dataset/")
                and "resource" in related_identifier
                and "download" in related_identifier
            ):
                resources_url.append({"url": related_identifier})

    return resources_url


def get_spatial_dc(geo_locations: list) -> (dict, str):
    """Returns spatial data in EnviDat formatted dictionary and spatial_info as string.

    Args:
         geo_locations (list): geoLocations list in DataCite record
    """
    spatial = {}
    spatial_info = ""
    geometries = []

    for location in geo_locations:
        if len(location) == 1:
            geo_type, geo_data = list(location.items())[0]

            # Assign spatial_info
            if geo_type == "geoLocationPlace":
                spatial_info = geo_data

            # Handle (single item) spatial data
            if len(geo_locations) <= 2:
                if geo_type == "geoLocationPoint":
                    if coords := get_point_coords_dc(geo_data):
                        spatial.update({"type": "Point", "coordinates": coords})

                if geo_type == "geoLocationPolygon":
                    if coords := get_polygon_coords_dc(geo_data):
                        spatial.update({"type": "Polygon", "coordinates": [coords]})

            # Handle GeometryCollection (multiple items) spatial data
            else:
                if geo_type == "geoLocationPoint":
                    if coords := get_point_coords_dc(geo_data):
                        geometries.append({"type": "Point", "coordinates": coords})

                if geo_type == "geoLocationPolygon":
                    if coords := get_polygon_coords_dc(geo_data):
                        geometries.append({"type": "Polygon", "coordinates": [coords]})

            if geometries:
                spatial.update({"type": "GeometryCollection", "geometries": geometries})

    return spatial, spatial_info


def get_point_coords_dc(geo_location_point: dict) -> list | None:
    """Returns DataCite geoLocationPoint dictionary as EnviDat formatted coordinate
    pair list.
    Output coordinates are converted to float and in [longitute, latitute] format.
    If conversion fails then returns None.

    Args:
        geo_location_point (dict): geoLocationPoint dictionary from DataCite record
    """
    longitude = geo_location_point.get("pointLongitude")
    latitude = geo_location_point.get("pointLatitude")

    if latitude and longitude:
        return [float(longitude), float(latitude)]

    return None


def get_polygon_coords_dc(geo_location_polygon: dict) -> list | None:
    """Returns DataCite geoLocationPolygon dictionary as EnviDat formatted coordinate
    pair list.
    In ouput coordinates are converted to float and in [longitute, latitute] format.
    If conversion fails then returns None.

    Args:
        geo_location_polygon (dict): geoLocationPoint dictionary from DataCite record
    """
    coordinates = []

    for coord in geo_location_polygon:
        if len(coord) == 1 and list(coord.keys())[0] == "polygonPoint":
            coord_pair = list(coord.values())[0]
            longitude = coord_pair.get("pointLongitude")
            latitude = coord_pair.get("pointLatitude")
            if longitude and latitude:
                coordinates.append([float(longitude), float(latitude)])

    if len(coordinates) >= 3:
        return coordinates

    return None
