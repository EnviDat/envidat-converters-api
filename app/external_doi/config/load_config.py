"""Load config to usedby external DOI converters."""

import json

import logging

log = logging.getLogger(__name__)


def load_config(
    config_path: str = "app/external_doi/config/config_external_doi.json",
) -> dict | None:
    """Return config. If fails then returns None.

    Args:
        config_path (str): path to JSON config file
    """
    try:
        with open(config_path, "r") as conf:
            return json.load(conf)
    except FileNotFoundError as e:
        log.error(f"ERROR: cannot not find config at path {config_path}, error {e}")
        return None
