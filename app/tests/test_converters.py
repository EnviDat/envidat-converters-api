from fastapi.testclient import TestClient
from app.main import app
import httpx


# Initalize TestClient used to test application
client = TestClient(app)


# Test class used to test api_converters.convert_envidat_package which is
#   used in endpoint "/convert/{converter}"
# Class has methods that test all supported converters
class TestConverters:
    prefix = "/internal-dataset/convert"
    package_id = "data-reliability-study-avalanche-size-and-outlines"

    def test_convert_envidat_package_bibtex(self):
        response = self.get_converter_response("bibtex")
        assert response.status_code == 200
        assert len(response.text) > 0

    def test_convert_envidat_package_datacite(self):
        response = self.get_converter_response("datacite")
        assert response.status_code == 200
        assert len(response.text) > 0

    def test_convert_envidat_package_dcat_ap(self):
        response = self.get_converter_response("dcat-ap")
        assert response.status_code == 200
        assert len(response.text) > 0

    def test_convert_envidat_package_dif(self):
        response = self.get_converter_response("dif")
        assert response.status_code == 200
        assert len(response.text) > 0

    def test_convert_envidat_package_ris(self):
        response = self.get_converter_response("ris")
        assert response.status_code == 200
        assert len(response.text) > 0

    def test_convert_envidat_package_iso(self):
        response = self.get_converter_response("iso")
        assert response.status_code == 200
        assert len(response.text) > 0

    def get_converter_response(self, converter: str) -> httpx.Response:
        url = f"{self.prefix}/{converter}?package-id={self.package_id}"
        return client.get(url)
