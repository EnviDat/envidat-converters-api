import json

import pytest as pytest
from fastapi.testclient import TestClient

from app.external_doi.config.load_config import load_config
from app.external_doi.datacite import convert_datacite_to_envidat
from app.external_doi.dryad import convert_dryad_to_envidat
from app.external_doi.zenodo import convert_zenodo_to_envidat
from app.main import app

# Initalize TestClient used to test application
client = TestClient(app)


# Test class used to test api_external_doi.convert_external_doi which is
#   used in endpoint "/external-doi/convert"
class TestExternalDOI:
    config = load_config()

    owner_org = "bd536a0f-d6ac-400e-923c-9dd351cb05fa"
    add_placeholders = True

    # Test data generated from DataCite DOI: https://doi.org/10.13093/permos-2016-01
    datacite_data = "datacite_v2_0_2024_03_08.json"
    datacite_converted = "datacite_converted_v0_1_1_2024_03_08.json"

    # Test data generated from Dryad DOI: https://doi.org/10.5061/dryad.866t1g1v6
    dryad_data = "dryad_v2_1_0_2024_03_07.json"
    dryad_converted = "dryad_converted_v0_1_1_2024_03_08.json"

    # Test data generated from Zenodo DOI: https://doi.org/10.5281/zenodo.6514932
    zenodo_data = "zenodo_2024_03_08.json"
    zenodo_converted = "zenodo_converted_v0_1_1_2024_03_08.json"

    def test_load_config(self):
        assert isinstance(self.config, dict)
        assert len(self.config) > 0

    def test_convert_external_doi_datacite(self):
        datacite_data = self.json_to_dict(self.datacite_data)
        datacite_converted = self.json_to_dict(self.datacite_converted)
        record = convert_datacite_to_envidat(
            datacite_data, self.owner_org, self.config, self.add_placeholders
        )
        assert datacite_converted == record

    @pytest.mark.asyncio
    async def test_convert_external_doi_dryad(self):
        dy_data = self.json_to_dict(self.dryad_data)
        dy_converted = self.json_to_dict(self.dryad_converted)
        record = await convert_dryad_to_envidat(
            dy_data, self.owner_org, self.config, self.add_placeholders
        )
        assert dy_converted == record

    def test_convert_external_doi_zenodo(self):
        zen_data = self.json_to_dict(self.zenodo_data)
        zen_converted = self.json_to_dict(self.zenodo_converted)
        record = convert_zenodo_to_envidat(
            zen_data, self.owner_org, self.config, self.add_placeholders
        )
        assert zen_converted == record

    @staticmethod
    def json_to_dict(name, file_dir: str = "app/tests/test_data") -> dict:
        """Return JSON file as Python dictionary.
        :param name: name of file
        :param file_dir: directory from root that file is in,
                            has default value of "app/tests/test_data"
        """
        file = f"{file_dir}/{name}"
        with open(file, "r", encoding="utf-8") as f:
            return json.load(f)
