{
  "doi": "10.5061/dryad.866t1g1v6",
  "title": "A range-wide postglacial history of Swiss stone pine based on molecular markers and palaeoecological evidence",
  "name": "a-range-wide-postglacial-history-of-swiss-stone-pine-based-on-molecular-markers",
  "author": "[{\"name\": \"Gugerli\", \"given_name\": \"Felix\", \"affiliation\": \"Swiss Federal Institute for Forest, Snow and Landscape Research\", \"email\": \"felix.gugerli@wsl.ch\", \"identifier\": \"0000-0003-3878-1845\"}, {\"name\": \"Brodbeck\", \"given_name\": \"Sabine\", \"affiliation\": \"Swiss Federal Institute for Forest, Snow and Landscape Research\", \"email\": \"sabine.brodbeck@wsl.ch\"}, {\"name\": \"Lendvay\", \"given_name\": \"Bertalan\", \"affiliation\": \"University of Zurich\", \"email\": \"bertalan.lendvay@imr.uzh.ch\"}, {\"name\": \"Dauphin\", \"given_name\": \"Benjamin\", \"affiliation\": \"Swiss Federal Institute for Forest, Snow and Landscape Research\", \"email\": \"benjamin.dauphin@wsl.ch\"}, {\"name\": \"Bagnioli\", \"given_name\": \"Francesca\", \"affiliation\": \"National Research Council\", \"email\": \"francesca.bagnoli@ibbr.cnr.it\"}, {\"name\": \"van der Knaap\", \"given_name\": \"Willem O.\", \"affiliation\": \"University of Bern\", \"email\": \"pim.vanderknaap@ips.unibe.ch\"}, {\"name\": \"Tinner\", \"given_name\": \"Willy\", \"affiliation\": \"University of Bern\", \"email\": \"willy.tinner@ips.unibe.ch\"}, {\"name\": \"Höhn\", \"given_name\": \"Maria\", \"affiliation\": \"Széchenyi István University\", \"email\": \"maria.hohn@uni-mate.hu\"}, {\"name\": \"Vendramin\", \"given_name\": \"Giovanni G.\", \"affiliation\": \"National Research Council\", \"email\": \"giovanni.vendramin@ibbr.cnr.it\"}, {\"name\": \"Morales-Molino\", \"given_name\": \"César\", \"affiliation\": \"University of Bern\", \"email\": \"cesar.morales@ips.unibe.ch\"}, {\"name\": \"Schwörer\", \"given_name\": \"Christoph\", \"affiliation\": \"University of Bern\", \"email\": \"christoph.schwoerer@ips.unibe.ch\"}]",
  "maintainer": "{\"name\": \"EnviDat\", \"email\": \"envidat@wsl.ch\"}",
  "owner_org": "bd536a0f-d6ac-400e-923c-9dd351cb05fa",
  "date": "[{\"date\": \"2023-02-20\", \"date_type\": \"created\"}]",
  "publication": "{\"publication_year\": \"2023\", \"publisher\": \"Dryad\"}",
  "funding": "[{\"institution\": \"Swiss National Science Foundation\", \"grant_number\": \"3100A0-113918\"}, {\"institution\": \"Swiss National Science Foundation\", \"grant_number\": \"31003A_152664\"}]",
  "language": "en",
  "private": true,
  "license_id": "CC0-1.0",
  "license_title": "Creative Commons Zero - No Rights Reserved (CC0 1.0)",
  "license_url": "https://creativecommons.org/publicdomain/zero/1.0/",
  "notes": "**Aim:**Knowing a species’ response to historical climate shifts helps understanding its perspectives under global warming.We infer the hitherto unresolved postglacial history of *Pinus cembra.* Using independent evidence from genetic structure and demographic inference of extant populations, and from palaeoecological findings, we derive putative refugia and re-colonisation routes.\n\n\n**Location:**European Alps and Carpathians.\n\n\n**Taxa:***Pinus cembra.*\n\n\n**Methods:**We genotyped nuclear and chloroplast microsatellite markers in nearly 3,000 individuals from 147 locations across the entire natural range of *P. cembra*. Spatial genetic structure (Bayesian modelling) and demographic history (Approximate Bayesian Computation) were combined with palaeobotanical records (pollen, macrofossils) to infer putative refugial areas during the Last Glacial Maximum (LGM) and re-colonisation of the current range.\n\n\n**Results:**We found distinct spatial genetic structure, despite low genetic differentiation even between the two disjunct mountain ranges. Nuclear markers revealed five genetic clusters aligned East–West across the range, while chloroplast haplotype distribution suggested nine clusters. Spatially congruent separation at both marker types highlighted two main genetic lineages in the East and West of the range. Demographic inference supported early separation of these lineages dating back to a previous interstadial or interglacial *c.* 210,000 years ago. Differentiation into five biologically meaningful genetic clusters likely established during post-glacial re-colonisation.\n\n\n**Main conclusions:**Combining genetic and palaeoecological evidence suggests that *P. cembra* primarily survived the LGM in “cold period” refugia south of the Central European Alps and near the Carpathians, from where it expanded during the Late Glacial into its current Holocene “warm period” refugia. This colonisation history has led to the distinct East–West structure of five genetic clusters. The two main genetic lineages likely derived from ancient divergence during an interglacial or interstadial. The respective contact zone (Brenner line) matches a main biogeographic break in the European Alps also found in herbaceous alpine plant species.\n\n Genotype file with individual genotypes from 11 nuclear microsatellites (2 lines per individual), prepared for input in the clustering software STRUCTURE (Pritchard et al., Genetics 2000), available at https://web.stanford.edu/group/pritchardlab/structure\\_software/release\\_versions/v2.3.4/html/structure.html:\n\n\nGugerli\\_etal\\_JoB2023\\_nSSRs\\_STRUCTURE.csv\n\n\nGugerli\\_etal\\_JoB2023\\_nSSRs\\_GenePop.csv\n\n\n \n\n\nThree files with data from 4 chloroplast microsatellites, formatted for input in the clustering software BAPS (Corander et al., Genetics 2003), currently under development as an R package at https://github.com/ocbe-uio/rBAPS:\n\n\nGugerli\\_etal\\_JoB2023\\_cpSSRs-BAPS\\_Pop\\_PopGroupsUnlinkedLoci.csv\n\n\nGugerli\\_etal\\_JoB2023\\_cpSSRs-BAPS\\_Pop\\_names.csv\n\n\nGugerli\\_etal\\_JoB2023\\_cpSSRs-BAPS\\_Pop\\_indices.csv\n\n\n \n\n\nPopulations file with information on sampling sites (population codes, mountain range, country, population name, geographic coordinates in WGS84 format) and sample numbers.\n\n\nGugerli\\_etal\\_JoB2023\\_PopulationSampling.csv",
  "related_publications": "* https://doi.org/10.1007/s11295-014-0770-9\r\n* https://doi.org/10.1111/j.1365-2699.2009.02122.x\r\n* https://doi.org/10.1007/s00035-009-0052-6\r\n* https://doi.org/10.1111/jbi.14586\r\n",
  "resource_type_general": "dataset",
  "spatial": "{\"type\": \"Point\", \"coordinates\": [8.4545978, 47.3606372]}",
  "tags": [
    {
      "name": "PINUS CEMBRA"
    },
    {
      "name": "PHYLOGEOGRAPHY"
    },
    {
      "name": "SPATIAL GENETIC STRUCTURE"
    },
    {
      "name": "GENETIC DIVERSITY"
    },
    {
      "name": "MICROSATELLITE GENOTYPES"
    },
    {
      "name": "POSTGLACIAL RECOLONIZATION"
    },
    {
      "name": "SWISS STONE PINE"
    },
    {
      "name": "ECOLOGY"
    },
    {
      "name": "ECOLOGY EVOLUTION BEHAVIOR AND SYSTEMATICS"
    }
  ],
  "version": "3"
}
