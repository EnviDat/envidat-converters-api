"""Utils for edna module."""

import json
import logging

from pydantic import BaseModel, AnyUrl, ValidationError

log = logging.getLogger(__name__)


def filter_packages_by_tag(package_list: list, tag_filter: str) -> list:
    """
    Return list of EnviDat public CKAN API packages with tag_filter.

    Args:
        package_list: List of EnviDat CKAN packages.
        tag_filter: Tag used to filter EnviDat datasets.
    """
    return [
        pkg
        for pkg in package_list
        for tag in pkg.get("tags", {})
        if tag.get("display_name") == tag_filter
    ]


def load_config(
    config_path: str = "app/edna/config_edna.json",
) -> dict | None:
    """Return config. If fails then returns None.

    Args:
        config_path: path to JSON config file
    """
    try:
        with open(config_path, "r") as conf:
            return json.load(conf)
    except FileNotFoundError as e:
        log.error(f"ERROR: cannot not find config at path {config_path}, error {e}")
        return None


class ConfigEdnaModel(BaseModel):
    """
    Class with required EDNA config values.
    """

    endpoint_url: AnyUrl
    bucket: str


def validate_edna_config(conf: dict) -> ConfigEdnaModel | str:
    """
    Return ConfigEdnaModel.
    If validation fails then return error string.

    Args:
        conf: Dictionary with configuration.
    """
    try:
        return ConfigEdnaModel(**conf)
    except ValidationError as e:
        return f"Failed to validate config, error: {e}"
    except ValueError as e:
        return f"Incorrect value in config, error: {e}"
    except Exception as e:
        return f"Other config validation error: {e}"


def is_valid_edna_s3_prefix(prefix: str) -> bool:
    """
    Return True is eDNA object prefix is in expected format.
    If prefix is invalid then returns False.

    Example valid prefix: "ma_fr/ma_fr_evhoe_2020"

    Args:
        prefix: Prefix string that will be validated.
    """
    parts = prefix.partition("/")
    if parts[0] and parts[2].startswith(parts[0]):
        return True
    return False


def get_edna_s3_prefix(pkg: dict) -> str | None:
    """
    Return eDNA prefix if it passes validation criteria.
    Else return None.

    Note: Only returns validated prefix for resource 'url' value from first resource!

    Example URL that fulfills validation:
    https://envicloud.wsl.ch/#/?bucket=https://envicloud.wsl.ch/edna&prefix=ma_fr/ma_fr_evhoe_2020

    Also accepts a URL that has "/" encoded as "%2F".

    Args:
        pkg: Dictionary in EnviDat CKAN format.
    """
    resources = pkg.get("resources", [])

    if len(resources) > 0:

        url_raw = resources[0].get("url")
        url = url_raw.replace("%2F", "/")

        prefix_substring = "prefix="
        substrings = ["/edna", prefix_substring]

        if all(x in url for x in substrings):

            url_parts = url.rpartition(prefix_substring)
            prefix = url_parts[2]

            if prefix.startswith("/"):
                prefix = prefix[1:]

            if prefix.endswith("/"):
                prefix = prefix[:-1]

            if is_valid_edna_s3_prefix(prefix):
                return prefix

    return None
