"""Functions used to process eDNA S3 objects."""

import boto3
from botocore import UNSIGNED
from botocore.client import Config
from botocore.exceptions import ClientError

import logging

from app.edna.utils import ConfigEdnaModel

log = logging.getLogger(__name__)


def get_s3_anonymous_client(endpoint: str):
    """
    Return S3 anonymous client.
    If fails then logs error and returns None.

    Args:
        endpoint: S3 endpoint URL to use to create boto3 client.
    """
    try:
        return boto3.client(
            "s3", config=Config(signature_version=UNSIGNED), endpoint_url=endpoint
        )
    except Exception as e:
        log.error(e)
        return None


def get_s3_objects_list(client, bucket: str, prefix: str = "") -> dict | None:
    """
    Returns response as dictionary of S3 objects in specified bucket
    (and optionally prefix).
    If fails then logs error and returns None.

    Args:
        client: boto3 client
        bucket: Name of bucket to list objects from.
        prefix: Optional. Prefix to use within bucket. Default to empty string "".
    """
    try:
        return client.list_objects_v2(Bucket=bucket, Prefix=prefix)
    except ClientError as e:
        log.error(e)
        return None


def parse_s3_response_keys(response: dict) -> list[str]:
    """
    Return list of keys in S3 response dictionary content.
    Expects response returned from S3 client.list_objects_v2()
    """
    keys = []
    for obj in response.get("Contents", []):
        if ky := obj.get("Key"):
            keys.append(ky)
    return keys


def get_s3_taxadata_csv_urls(
    client: boto3.client, config: ConfigEdnaModel, prefix: str
) -> list[str] | None:
    """
    Return list of eDNA taxadata csv file URLs.
    If error then returns None.

    Args:
        client: boto3 client
        config: Validated config model.
        prefix: Parsed S3 object prefix that corresponds to eDNA expedition.
    """
    endpoint = str(config.endpoint_url)
    prefix_taxadata = f"{prefix}/taxadata/"

    response = get_s3_objects_list(client, config.bucket, prefix_taxadata)
    if not response:
        return None

    keys = parse_s3_response_keys(response)

    taxadata_csv_urls = []
    for key in keys:
        if key.endswith(".csv"):
            taxadata_csv_urls.append(f"{endpoint}{config.bucket}/{key}")

    return taxadata_csv_urls


def get_s3_rawdata_objects(
    client: boto3.client, config: ConfigEdnaModel, prefix: str
) -> list[str] | None:
    """
    Return list of eDNA rawdata objects.
    If error then returns None.

    Args:
        client: boto3 client
        config: Validated config model.
        prefix: Parsed S3 object prefix that corresponds to eDNA expedition.
    """
    prefix_rawdata = f"{prefix}/rawdata/"

    response = get_s3_objects_list(client, config.bucket, prefix_rawdata)
    if not response:
        return None

    keys = parse_s3_response_keys(response)

    return keys


def is_edna_object(edna_objects: list[str], url: str) -> bool:
    """
    Return True if url matches pattern in an item in edna_objects.
    Else return False.

    Args:
         edna_objects: List of edna bucket objects. Can be a sublist of objects
            in a directory like the output produced by get_s3_rawdata_objects()
         url: String that has URL that needs to be validated.
    """
    parts = url.rpartition("edna/")

    if parts[0]:
        url_end = parts[2]
        if url_end in edna_objects:
            return True

    return False
