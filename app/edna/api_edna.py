"""Router used to convert eDNA related EnviDat CKAN packages to 'shallow datasets'
consumable by EnviDat frontend."""

import logging
from typing import Annotated

from fastapi import APIRouter, HTTPException, Query
from fastapi.responses import JSONResponse

from app.edna.s3_access import (
    get_s3_taxadata_csv_urls,
    get_s3_rawdata_objects,
    get_s3_anonymous_client,
)
from app.edna.formatters import process_shallow_datasets
from app.edna.utils import (
    filter_packages_by_tag,
    load_config,
    get_edna_s3_prefix,
    validate_edna_config,
    ConfigEdnaModel,
)
from app.remote_ckan import get_ckan_public, ckan_current_package_list_with_resources

log = logging.getLogger(__name__)

router = APIRouter(prefix="/edna", tags=["edna"])


@router.get(
    "/shallow-datasets",
    name="Generate eDNA shallow datasets",
    responses={
        200: {
            "description": "eDNA EnviDat datasets successfully converted to "
            "'shallow dataset' format"
        }
    },
)
def get_edna_shallow_datasets(
    tag: Annotated[
        str, Query(description="tag used to filter EnviDat datasets")
    ] = "EDNA"
) -> JSONResponse:
    """
    Generate eDNA related 'shallow datasets' in a format
    consumable by EnviDat frontend special eDNA data view.
    """
    conf_raw = load_config("app/edna/config_edna.json")
    config = validate_edna_config(conf_raw)
    if not isinstance(config, ConfigEdnaModel):
        log.error(config)
        raise HTTPException(status_code=500, detail="Configuration error, check logs")

    endpoint = str(config.endpoint_url)
    client = get_s3_anonymous_client(endpoint)
    if not client:
        raise HTTPException(status_code=500, detail="S3 client error, check logs")

    # Calling all CKAN packages is necessary because CKAN action 'tag_show' formats
    # EnviDat custom fields in 'extras' dictionary, therefore it is not compatible with
    # formatting functions compatible with EnviDat format
    ckan = get_ckan_public()
    package_list = ckan_current_package_list_with_resources(ckan)

    edna_packages = filter_packages_by_tag(package_list, tag)
    if not edna_packages:
        log.error(f"No EnviDat datasets found with tag '{tag}'")
        raise HTTPException(status_code=204)

    shallow_datasets = []
    for pkg in edna_packages:
        prefix = get_edna_s3_prefix(pkg)
        if not prefix:
            log.warning(
                f"First resource 'url' does not have valid eDNA bucket prefix "
                f"for EnviDat package '{pkg.get('name')}'"
            )
            continue

        taxadata_csv_files = get_s3_taxadata_csv_urls(client, config, prefix)
        if isinstance(taxadata_csv_files, list) and len(taxadata_csv_files) == 0:
            log.warning(
                f"No csv files found in eDNA expedition taxadata directory "
                f"for EnviDat package '{pkg.get('name')}'"
            )
            continue
        if not taxadata_csv_files:
            log.warning(
                f"Failed to extract list of csv files in eDNA expedition taxadata "
                f"directory for EnviDat package '{pkg.get('name')}', check logs"
            )
            continue

        rawdata_objects = get_s3_rawdata_objects(client, config, prefix)
        if not rawdata_objects:
            log.warning(
                f"Failed to extract objects in eDNA expedition rawdata "
                f"directory for EnviDat package '{pkg.get('name')}', check logs"
            )
            continue

        # Generate the shallow datasets
        shallow = process_shallow_datasets(pkg, taxadata_csv_files, rawdata_objects)
        shallow_datasets += shallow

    return JSONResponse(content=shallow_datasets)
