# Imports
# Setup logging
import json
import logging
import re
import pandas as pd

from app.edna.s3_access import is_edna_object

# Setup program logging to console (terminal)
# Check terminal for logged information, warning and error messages
log = logging.getLogger(__name__)


def build_taxa_tags(t_data: pd.DataFrame, taxa_dict: dict) -> dict | None:
    """Formatting tags collected from Taxadata csv for each event_code.

    Args:
        t_data(list): A Pandas dataframe containing required fields from
                      Taxadata CSV file.

        taxa_dict(dict): A dictionary to store occurrenceID(s), latitude
                         and longitude keyed by eventID.

    Returns:
        dict: Dictionary of tags keyed by each eventID
    """
    if not {"occurrenceID", "eventID", "decimalLatitude", "decimalLongitude"}.issubset(
        t_data.columns
    ):
        log.warning(
            "Required columns('occurrenceID', 'eventID', 'decimalLatitude', 'decimalLongitude') missing for "
            "Taxadata."
        )
        return None

    group_taxa = t_data.groupby("eventID")
    for event, group in group_taxa:
        occid = []
        for item in group["occurrenceID"]:
            try:
                pattern = r"(?<=:)[^;]+|(?<=;)[^;]+"
                match = re.findall(pattern, item)
                # remove the trailing number
                if match:
                    match[-1] = re.sub(r"-\d+$", "", match[-1])
                # remove NA
                match = list(filter(lambda x: x != "NA", match))
                occid.extend(match)
            except re.error as e:
                log.error(
                    f"Error occurred while reading occurrenceIDs,"
                    f"error: {e.msg} with Pattern: {e.pattern} and Position: {e.pos}"
                )

        if taxa_dict.get(event) is None:
            taxa_dict[event] = {
                "occurrenceID": list(set(occid)),
                "decimalLatitude": group["decimalLatitude"].iloc[0],
                "decimalLongitude": group["decimalLongitude"].iloc[0],
            }
        else:
            if (
                taxa_dict[event]["decimalLatitude"] == float("inf")
                and group["decimalLatitude"].iloc[0]
            ):
                taxa_dict[event]["decimalLatitude"] = group["decimalLatitude"].iloc[0]
            if (
                taxa_dict[event]["decimalLongitude"] == float("inf")
                and group["decimalLongitude"].iloc[0]
            ):
                taxa_dict[event]["decimalLongitude"] = group["decimalLongitude"].iloc[0]
            taxa_dict[event]["occurrenceID"] = list(
                set(taxa_dict[event]["occurrenceID"] + occid)
            )

    return taxa_dict


def format_person_details(person: dict) -> dict | None:
    """Formatting authors and maintainers to store only relevant details.

    Args:
        person(dict): A dict containing details of a person.

    Returns:
        dict: Dictionary having person details.
    """
    person_dict = dict(
        (key, person[key])
        for key in ["email", "given_name", "identifier", "name"]
        if key in person
    )
    return person_dict


def process_shallow_datasets(
    dset: dict, urls: list, rawdata_objects: list
) -> list[dict] | None:
    """Formatting resources(s) to have only required details for the shallow datasets.

    Args:
        dset(dict): The CKAN package from which shallow datasets are created.
        urls (list): List of URLs of taxadata CSV files from a single CKAN package with eDNA resources.
        rawdata_objects(list): List of objects present in the EDNA bucket under the project, to validate against.

    Returns:
        list: A list of shallow datasets for that CKAN package
    """
    taxa_dict = {}
    base_url = ""
    subdir = []
    shallow_list = []
    try:
        # Building the taxa data
        for url in urls:
            t_data = pd.read_csv(url, encoding="utf-8")
            t_data = t_data[
                ["occurrenceID", "eventID", "decimalLatitude", "decimalLongitude"]
            ]
            fill_navalues = {
                "occurrenceID": "",
                "eventID": "",
                "decimalLatitude": float("inf"),
                "decimalLongitude": float("inf"),
            }
            t_data.fillna(fill_navalues, inplace=True)
            build_taxa_tags(t_data, taxa_dict)

            # get subdirectories from url
            link = re.search("^(.+?)/taxadata/.+_(.+).csv", url)
            base_url = link.group(1)
            subdir.append(link.group(2))

            if taxa_dict is None:
                log.warning("Failed to fetch either Taxadata tags/coordinates.")
                return None
    except Exception as e:
        log.error(f"Error collecting details from taxadata file," f"error: {e}")

    # capturing only relevant author details
    author_details = json.loads(dset["author"])
    authors = []
    for item in author_details:
        author = format_person_details(item)
        authors.append(author)
    maintainers = format_person_details(json.loads(dset["maintainer"]))

    # creating shallow datasets from each filter_code
    for event in taxa_dict.keys():
        try:
            dataset = {}
            dataset["author"] = dset["author"]  # authors
            dataset["date"] = dset["date"]
            dataset["doi"] = dset["doi"]

            if extras := dset.get("extras"):
                dataset["extras"] = extras

            dataset["funding"] = dset["funding"]
            dataset["id"] = event.lower() + f"_{dset['id']}"
            dataset["license_title"] = dset["license_title"]
            dataset["license_url"] = dset["license_url"]
            dataset["maintainer"] = maintainers

            # Add timestamp fields if they exist
            if metadata_created := dset.get("metadata_created"):
                dataset["metadata_created"] = metadata_created

            if metadata_modified := dset.get("metadata_modified"):
                dataset["metadata_modified"] = metadata_modified

            dataset["name"] = f"{event}_{dset['name']}"
            # Changing the description
            dataset[
                "notes"
            ] = f"Sample {event} of dataset: <https://www.doi.org/{dset['doi']}>"
            # storing id, name, title from organization
            dataset["organization"] = {
                "id": dset["organization"]["id"],
                "name": dset["organization"]["name"],
                "title": dset["organization"]["title"],
            }

            dataset["publication"] = dset["publication"]

            # Add optional fields if they exist
            if related_datasets := dset.get("related_datasets"):
                dataset["related_datasets"] = related_datasets
            
            if related_publications := dset.get("related_publications"):
                dataset["related_publications"] = related_publications

            # processing resources
            dataset["resources"] = []
            for folder in subdir:
                raw_url = base_url + "/rawdata/" + folder + "/" + event + ".tar.gz"
                # check if the s3 object exists
                if is_edna_object(rawdata_objects, raw_url):
                    dataset["resources"].append({"url": raw_url})

            # formatting spatial geometry
            dataset["spatial"] = {}
            dataset["spatial"]["type"] = "Point"
            coord = [
                taxa_dict[event]["decimalLongitude"],
                taxa_dict[event]["decimalLatitude"],
            ]
            dataset["spatial"]["coordinates"] = coord
            # if coordinates are not present do not add the shallow dataset
            if any(element == float("inf") for element in coord):
                continue

            # Add optional field "spatial_info" if it exists
            if spatial_info := dset.get("spatial_info"):
                dataset["spatial_info"] = spatial_info

            # storing tags collected from metadata and taxadata
            tags = [
                {"name": x.upper()}
                for x in taxa_dict[event]["occurrenceID"]
                if pd.isnull(x) is not True
            ]
            dataset["tags"] = dset["tags"] + tags
            dataset["title"] = f"{event} - {dset['title']}"
            shallow_list.append(dataset)
        except KeyError as e:
            log.error(f"Cannot find the required key in event {event}, error: {e}")
        except Exception as e:
            log.error(e)

    return shallow_list
