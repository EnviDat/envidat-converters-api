# envidat-converters-api

#### API that converts EnviDat datasets to other metadata formats.

#### API also converts datasets hosted on external platforms to EnviDat format.

[EnviDat](https://www.envidat.ch) is the environmental data portal of the Swiss Federal Institute for Forest, Snow and Landscape Research WSL.

Project uses the FastAPI framework. [Click here for the official FastAPI documentation.](https://fastapi.tiangolo.com/)

## Documentation Topics

> - [API Swagger Docs](#api-swagger-docs-and-openapi-specification)
> - [OpenAPI Specification](#api-swagger-docs-and-openapi-specification)
> - [Converters Overview Diagram](#converters-overview-diagram)
> - [Development Usage](#development-usage)
> - [Production Usage](#production-usage)
> - [eDNA](#edna)
> - [Pre-commit hooks](#pre-commit-hooks)
> - [Tests](#tests)
> - [Scripts](#scripts)
> - [Authors](#authors)
> - [License](#license)

## API Swagger Docs and OpenAPI Specification

- Swagger documentation of API endpoints: https://www.envidat.ch/converters-api/docs
- OpenAPI specification: https://www.envidat.ch/converters-api/openapi.json

## Converters Overview Diagram

Diagram displays currently supported external platforms and metadata formats. \
Click here to see an example of a [dataset in EnviDat format.](https://www.envidat.ch/api/action/package_show?id=in-situ-soil-moisture-measurements-napf-region-2019-2022)

![image](/conversion_overview_diagram.png)

## Development Usage

1. Configure environment variables for use **only in local development**

   - Create `.env` file in project root directory
   - For example configuration see `env.example`
   - All environment variables in `env.example` are required except `ROOT_PATH` ([for more infomation see Production Usage](#production-usage))
   - **Never** commit the `.env` file
   - Environment variables and their types are specified in the Pydantic model `ConfigApp Model` in `app/config.py`
     - `ConfigAppModel` should be updated as needed because it used for validation

2. Create a new virtual environment, activate it, and install dependencies from `requirements.txt`:

   ```bash
    pip install virtualenv
    python -m venv <virtual-environment-name>
    <virtual-environment-name>/Scripts/activate
    python -m pip install -r requirements.txt
   ```

3. Run the FastAPI server:

   ```bash
   uvicorn app.main:app --port 8000 --reload
   ```

   Or alternatively:

   ```bash
   fastapi dev app/main.py --reload
   ```

4. Access local server at: http://127.0.0.1:8000

## Production Usage

1. Configure environment variables used in production

   - Configure CI/CD variables used to log in into production server: `DEPLOY_HOSTNAME`, `DEPLOY_SSH_KEY`, and `DEPLOY_SSH_USER`
   - Create **individual CI/CD variables for each variable** listed in `env.example`
   - `APP_VERSION` **must be incremented** so that a new image is built and the application includes the updated code
     - Create a git tag for the commit that corresponds to the `APP_VERSION`
   - `ROOT_PATH` is an optional environment variable and should only be used to if the application uses a proxy
     - Be sure to include a `/` before the `ROOT_PATH` value
     - Example configuration: `ROOT_PATH=/converters-api`
     - [Click here for the FastAPI documentation about using a proxy server](https://fastapi.tiangolo.com/advanced/behind-a-proxy/)
   - Create **individual CI/CD variables for each the following variables** that are used for deployment:

     > | Key              | Example Value                    |
     > | ---------------- | -------------------------------- |
     > | `INTERNAL_REG`   | `registry-gitlab.org.ch/orgname` |
     > | `EXTERNAL_REG`   | `docker.io`                      |
     > | `NGINX_IMG_TAG`  | `1.25`                           |
     > | `PYTHON_IMG_TAG` | `3.11`                           |

2. Merge feature/development branch to `main` default branch
   - The `main` branch has a pipeline set up in `.gitlab-ci.yml` that automatically deploys changes to production server
   - The pipeline also requires CI/CD variables that are used to that are used to build and register an image: `IMAGE_REGISTRY_USER` and `IMAGE_REGISTRY_PASS`
     - The image related variables can be group variables inherited from the parent group

## eDNA

- Endpoint generates the shallow datasets used for the [EnviDat special eDNA Data view.](https://www.envidat.ch/#/browse?mode=edna)
- Each shallow dataset is constructed from every eventID present in eDNA datasets' taxadata files.
- Module: `app/edna`
- API Endpoint:
  - **/edna/shallow-datasets**
- API Endpoint with optional tag filter:
  - **/edna/shallow-datasets?tag=EDNA**
  - If tag is omitted then "EDNA" is the default tag used to filter EnviDat datasets.

#### Configuration

The edna module uses a configuration file: `app/edna/config_edna.json`

| Required Configuration Keys | Description                                     |
| --------------------------- | ----------------------------------------------- |
| `"endpoint_url"`            | S3 endpoint that hosts eDNA project data bucket |
| `"bucket"`                  | Name of bucket containing eDNA project objects  |

Example `config_edna.json`:

```
   {
        "endpoint_url": "https://os.zhdk.cloud.switch.ch",
        "bucket": "edna"
   }
```

The configuration example above is used for the to access edna S3 bucket objects processed by the EnviDat special eDNA Data view.

## Pre-commit hooks

- The pre-commit hooks run every time you attempt to commit files
- To run the pre-commit hooks manually open app in terminal and execute: `pre-commit run --all-files`
- These hooks ensure that the application uses standard stylistic conventions
- To view or alter the pre-commit hooks see: `.pre-commit-config.yaml`

## Tests

- Tests are located in `app/tests`
- Tests are run during `test` stage of the pipeline, please see `.gitlab-ci.yml`
- To run tests manually open app in terminal and execute: `pytest`

## Scripts

Scripts and their associated logs are located in the `scripts` directory

| Script                        | Purpose                                                                                                                                                       |
| ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `datasets_stewardship.py`     | Return a list of EnviDat datasets filtered by supported conditions                                                                                            |
| `datasets_filter.py`          | Return a list of EnviDat datasets that are filtered by a supported filters applied to _one_ field                                                             |
| `datasets_filter_multiple.py` | Return a list of EnviDat datasets that are filtered by matches to values in _several_ supported fields, <br/>optionally can patch datasets that match filters |
| `zenodo_import.py`            | Convert and bulk import multiple Zenodo records                                                                                                               |

## Authors

**The following employees of the Swiss Federal Institute for Forest, Snow and Landscape Research WSL:**

[Rebecca Kurup Buchholz](https://www.linkedin.com/in/rebeccakurupbuchholz/) \
Ranita Pal \
Sam Woodcock \
Yasmin Waldeck

## License

[MIT License](https://gitlabext.wsl.ch/EnviDat/envidat-converters-api/-/blob/main/LICENSE?ref_type=heads)
